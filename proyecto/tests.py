from django.test import TestCase
from django.contrib.auth.models import User
from proyecto.models import *
from proyecto import forms

class ProyectoTest(TestCase):

    def crear_proyecto(self, nombre_corto='Test', nombre_largo='Proyecto Test', estado='IN', descripcion='Descripcion de prueba'):
        """
        Crea un proyecto de prueba
        @param nombre_corto:
        @param nombre_largo:
        @param estado:
        @param descripcion:
        @return: Instancia de un proyecto
        """
        return Proyecto.objects.create(nombre_corto=nombre_corto, nombre_largo=nombre_largo, estado=estado, descripcion=descripcion)

    def test_modelo_proyecto(self):
        """
        Prueba el modelo del proyecto
        """
        p = self.crear_proyecto()
        self.assertTrue(isinstance(p, Proyecto), "No es una instancia de Proyecto")

    def test_modelo_unicode(self):
        p = self.crear_proyecto()
        self.assertEqual(p.__unicode__(), p.nombre_corto, "Unicode diferente al nombre_corto del Proyecto")

    def test_modelo_descripcion(self):
        p = self.crear_proyecto()
        self.assertEqual(p.get_descripcion(), p.descripcion, "get_descripcion no coincide con la descripcion del Proyecto")

    def test_modelo_estado(self):
        p = self.crear_proyecto()
        self.assertEqual(p.get_estado(), p.estado, "get_estado no coincide con el estado del Proyecto")

    def test_form_valido(self):
        """
        Prueba un formulario valido
        """
        p = self.crear_proyecto()
        datos = {'nombre_corto_form': p.nombre_corto, 'nombre_largo_form': p.nombre_largo, 'descripcion_form': p.descripcion, 'scrum_form': 'unUsuario', 'estado_form': p.estado}
        formulario = forms.FormCrearProyecto(data=datos)
        self.assertTrue(formulario.is_valid(), "Formulario FormCrearProyecto: No es un formulario valido de actividad")

    def test_form_invalido_blank(self):
        """
        Prueba un formulario invalido
        """
        p = self.crear_proyecto()
        datos = {'nombre_corto_form': p.nombre_corto, 'nombre_largo_form': p.nombre_largo, 'descripcion_form': p.descripcion, 'scrum_form': '', 'estado_form': p.estado}
        formulario = forms.FormCrearProyecto(data=datos)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearProyecto: No es un formulario invalido con una cadena vacia")

    def test_form_invalido_null(self):
        """
        Prueba un formulario invalido
        """
        p = self.crear_proyecto()
        datos = {'nombre_corto_form': p.nombre_corto, 'nombre_largo_form': p.nombre_largo, 'descripcion_form': p.descripcion, 'scrum_form': None, 'estado_form': p.estado}
        formulario = forms.FormCrearProyecto(data=datos)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearProyecto: No es un formulario invalido con una cadena nula")

    def test_form_incompleto(self):
        """
        Prueba un formulario invalido
        """
        p = self.crear_proyecto()
        datos = {'nombre_corto_form': p.nombre_corto, 'nombre_largo_form': p.nombre_largo, 'descripcion_form': p.descripcion}
        formulario = forms.FormCrearProyecto(data=datos)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearProyecto: No es un formulario incompleto")


class UserStoryTest(TestCase):

    def crear_userstory(self, descripcion_corta='US Test', descripcion_larga='UserStory Test', prioridad='ME', valor_de_negocios=5, valor_tecnico = 5):
        """
        Crea un US de prueba
        @param descripcion_corta:
        @param descripcion_larga:
        @param prioridad:
        @return: Instancia de un UserStory de prueba
        """
        return UserStory.objects.create(descripcion_corta=descripcion_corta, descripcion_larga=descripcion_larga, prioridad=prioridad, valor_de_negocios= valor_de_negocios, valor_tecnico= valor_tecnico)

    def test_modelo_userstory(self):
        """
        Prueba el modelo del UserStory
        """
        us = self.crear_userstory()
        self.assertTrue(isinstance(us, UserStory), "No es una instancia de UserStory")

    def test_modelo_unicode(self):
        us = self.crear_userstory()
        self.assertEqual(us.__unicode__(), us.descripcion_corta, "Unicode diferente a descripcion_corta del US")

    def test_modelo_descripcion(self):
        us = self.crear_userstory()
        self.assertEqual(us.get_descripcion(), us.descripcion_larga, "get_descripcion diferente a descipcion_larga del US")

    def test_modelo_prioridad(self):
        us = self.crear_userstory()
        self.assertEqual(us.get_prioridad(), us.prioridad, "get_priodidad diferente a prioridad de US")

    def test_form_valido(self):
        """
        Prueba un formulario valido
        """
        us = self.crear_userstory()
        datos = {'descripcion_corta_form': us.descripcion_corta, 'descripcion_larga_form': us.descripcion_larga, 'tiempo_estimado_form': 10.5, 'prioridad_form': us.prioridad, 'valor_de_negocios_form':us.valor_de_negocios, 'valor_tecnico_form': us.valor_tecnico}
        formulario = forms.FormCrearUserStory(data=datos)
        self.assertTrue(formulario.is_valid(), "Formulario FormCrearUserStory: No es un formulario valido de actividad")

    def test_form_invalido_incompleto(self):
        """
        Prueba un formulario invalido
        """
        us = self.crear_userstory()
        datos = {'descripcion_corta_form': us.descripcion_corta, 'descripcion_larga_form': us.descripcion_larga, 'tiempo_estimado_form': us.tiempo_estimado, 'prioridad_form': us.prioridad}
        formulario = forms.FormCrearUserStory(data=datos)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearUserStory: No es un formulario incompleto")

    def test_form_invalido_blank(self):
        """
        Prueba un formulario valido
        """
        us = self.crear_userstory()
        datos = {'descripcion_corta_form': us.descripcion_corta, 'descripcion_larga_form': us.descripcion_larga, 'tiempo_estimado_form': 10.5, 'prioridad_form': us.prioridad, 'valor_de_negocios_form':us.valor_de_negocios, 'valor_tecnico_form': ''}
        formulario = forms.FormCrearUserStory(data=datos)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearUserStory: No es un formulario invalido con cadena vacia")

    def test_form_invalido_null(self):
        """
        Prueba un formulario valido
        """
        us = self.crear_userstory()
        datos = {'descripcion_corta_form': us.descripcion_corta, 'descripcion_larga_form': us.descripcion_larga, 'tiempo_estimado_form': 10.5, 'prioridad_form': us.prioridad, 'valor_de_negocios_form':us.valor_de_negocios, 'valor_tecnico_form': None}
        formulario = forms.FormCrearUserStory(data=datos)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearUserStory: No es un formulario invalido con cadena nula")

class PermisoTest(TestCase):

    def crear_permiso(self, nombre):
        """
        Crea un permiso de prueba
        @param nombre:
        @return: Instancia de un permiso de prueba
        """
        return Permiso.objects.create(nombre=nombre)

    def test_modelo_permiso(self):
        """
        Test del modelo de un permiso
        """
        p = self.crear_permiso('prueba_permiso')
        self.assertTrue(isinstance(p, Permiso), "No es una instancia de Permiso")

    def test_modelo_unicode(self):
        p = self.crear_permiso('prueba_permiso')
        self.assertEqual(p.__unicode__(), p.nombre, "Unicode diferente al nombre del Permiso")

    def test_form_valido(self):
        """
        Prueba un formulario valido
        """
        p = self.crear_permiso('prueba_permiso')
        dato = {'nombre': p.nombre}
        formulario = forms.FormEditarPermiso(data=dato)
        self.assertTrue(formulario.is_valid(), "Formulario FormEditarPermiso: No es un formulario valido de actividad")

    def test_form_invalido_blank(self):
        """
        Prueba un formulario invalido
        """
        dato = {'nombre': ''}
        formulario = forms.FormEditarPermiso(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario FormEditarPermiso: No es un formulario invalido con una cadena vacia")

    def test_form_invalido_null(self):
        dato = {'nombre': None}
        formulario = forms.FormEditarPermiso(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario FormEditarPermiso: No es un formulario invalido con una cadena nula")

    def test_form_incompleto(self):
        dato = {}
        formulario = forms.FormEditarPermiso(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario FormEditarPermiso: No es un formulario incompleto")

class RolTest(TestCase):


    def crear_permiso(self, nombre):
        """
        Instancia un nuevo permiso
        @rtype: object
        """
        return Permiso.objects.create(nombre=nombre)

    def crear_rol_de_prueba(self, nombre):
        """
        Funcion para la creacion de un rol de prueba con permisos asociados
        @return: Rol de prueba
        """
        lista_permisos = []

        # se crean permisos en un ciclo y se almacenan en una lista
        for i in range(1, 3):
            permiso = 'permiso_0' + str(i)                              # permisos de prueba del 1 al 3
            lista_permisos.append(self.crear_permiso(nombre=permiso))

        rol = Rol.objects.create(nombre=nombre)                  # instancia del rol de prueba

        for permiso in lista_permisos:
            rp = Rol_Permiso.objects.create(permiso=permiso, rol=rol)

        return rol

    def test_modelo_rol(self):
        rol = self.crear_rol_de_prueba(nombre='RolDePrueba')
        self.assertTrue(isinstance(rol, Rol), "No es una instancia de Rol")

    def test_modelo_unicode(self):
        rol = self.crear_rol_de_prueba(nombre='RolDePrueba')
        self.assertEqual(rol.__unicode__(), rol.nombre, "Unicode diferente al nombre del Rol")

    def test_modelo_rol_tiene_permisos(self):
        rol = self.crear_rol_de_prueba(nombre='RolDePrueba')
        self.assertNotEqual(rol.permisos, None, "Rol no tiene permisos asociados")

    def test_form_valido(self):
        rol = self.crear_rol_de_prueba(nombre='RolDePrueba')
        dato = {'nombre': rol.nombre}
        formulario = forms.FormEditarRol(data=dato)
        self.assertTrue(formulario.is_valid(), "Formulario FormEditarRol: No es un formulario valido de actividad")

    def test_form_invalido_blank(self):
        dato = {'nombre': ''}
        formulario = forms.FormEditarRol(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario FormEditarRol: No es un formulario invalido con una cadena vacia")

    def test_form_invalido_null(self):
        dato = {'nombre': None}
        formulario = forms.FormEditarRol(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario FormEditarRol: No es un formulario invalido con una cadena nula")

class SprintTest(TestCase):
    def crear_sprint(self):
        return Sprint.objects.create()

    def test_modelo_sprint(self):
        sprint = self.crear_sprint()
        self.assertTrue(isinstance(sprint, Sprint), "No es una instancia de Sprint")

    def test_modelo_get_estado(self):
        sprint = self.crear_sprint()
        self.assertEqual(sprint.get_estado_verbose(), 'Planificado', "Estado invalido del Sprint")

    def test_form_valido(self):
        sprint = self.crear_sprint()
        dato = {'duracion_dias_form': sprint.duracion_dias}
        formulario = forms.FormCrearSprint(data=dato)
        self.assertTrue(formulario.is_valid(), "Formulario FormCrearSprint: No es un formulario valido de actividad")

    def test_form_invalido_blank(self):
        dato = {'duracion_horas_form': ''}
        formulario = forms.FormCrearSprint(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearSprint: No es un formulario invalido con una cadena vacia")

    def test_form_invalido_null(self):
        dato = {'duracion_horas_form': None}
        formulario = forms.FormCrearSprint(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario FormCrearSprint: No es un formulario invalido con una cadena nula")

class FlujoActividadTest(TestCase):
    def crear_proyecto(self, nombre_corto='Test', nombre_largo='Proyecto Test', estado='IN', descripcion='Descripcion de prueba'):
        """
        Crea un proyecto de prueba
        @param nombre_corto:
        @param nombre_largo:
        @param estado:
        @param descripcion:
        @return: Instancia de un proyecto
        """
        return Proyecto.objects.create(nombre_corto=nombre_corto, nombre_largo=nombre_largo, estado=estado, descripcion=descripcion)


    def crear_flujo(self, descripcion):
        return Flujo_Proyecto.objects.create(descripcion=descripcion, proyecto=self.crear_proyecto())

    def crear_actividad(self, flujo, orden, descripcion):
        return Actividad.objects.create(flujo=flujo, orden=orden, descripcion=descripcion)

    def test_modelo_flujo(self):
        f = self.crear_flujo("Flujo_Proyecto de prueba")
        self.assertTrue(isinstance(f, Flujo_Proyecto), "No es una instancia de Flujo_Proyecto")

    def test_modelo_actividad(self):
        f = self.crear_flujo("Flujo_Proyecto de prueba")
        a = self.crear_actividad(f, 1, 'Actividad_01')
        self.assertTrue(isinstance(a, Actividad), "No es una instancia de Actividad")

    def test_form_actividad_valido(self,):
        flujo = self.crear_flujo("Flujo_Proyecto de prueba")
        a = self.crear_actividad(flujo, 1, 'Actividad_01')
        dato = {'idFlujo': flujo.id, 'orden': a.orden, 'estado': 'Doing', 'descripcion': a.descripcion}
        formulario = forms.FormCrearActividad(data=dato)
        self.assertTrue(formulario.is_valid(), "Formulario Actividad: No es un formulario valido de actividad")

    def test_form_actividad_invalido_blank(self):
        flujo = self.crear_flujo("Flujo_Proyecto de prueba")
        a = self.crear_actividad(flujo, 1, 'Actividad_01')
        dato = {'idFlujo': flujo.id, 'orden': a.orden, 'estado': '', 'descripcion': a.descripcion}
        formulario = forms.FormCrearActividad(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario Actividad: No es un formulario invalido con una cadena vacia")


    def test_form_actividad_invalido_null(self):
        flujo = self.crear_flujo("Flujo_Proyecto de prueba")
        a = self.crear_actividad(flujo, 1, 'Actividad_01')
        dato = {'idFlujo': flujo.id, 'orden': a.orden, 'estado': None, 'descripcion': a.descripcion}
        formulario = forms.FormCrearActividad(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario Actividad: No es un formulario invalido con una cadena nula")

    def test_form_actividad_incompleto(self):
        flujo = self.crear_flujo("Flujo_Proyecto de prueba")
        a = self.crear_actividad(flujo, 1, 'Actividad_01')
        dato = {'idFlujo': flujo.id, 'orden': a.orden, 'descripcion': a.descripcion}
        formulario = forms.FormCrearActividad(data=dato)
        self.assertFalse(formulario.is_valid(), "Formulario Actividad: No es un formulario incompleto")
