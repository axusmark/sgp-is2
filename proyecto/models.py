#!/usr/bin/env python

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django.db.models import Max
import time
from django.db.models import Max
class UserStory(models.Model):
    descripcion_corta = models.CharField(max_length=140)        # Breve descripcion del US
    descripcion_larga = models.CharField(max_length=500)                      # Descripcion mas detallada del US
    tiempo_estimado = models.FloatField(default=0.0)  # Tiempo estimado que tomara desarrollar el sprint
    tiempo_dedicado = models.FloatField(default=0.0)  # Tiempo dedicado por un Developer en el desarrollo del US
    prioridades_opciones = (('AL', 'Alta'), ('ME', 'Media'), ('BA', 'Baja'))    # Los tipos de prioridades que puede tener un US
    prioridad = models.CharField(choices=prioridades_opciones, max_length=2, default='ME') # Por default su prioridad sera media
    valor_de_negocios = models.FloatField(default=0.0)   # El valor de negocios que tendra un US
    valor_tecnico = models.FloatField(default=0.0)   # El valor tecnico de un US
    timestamp = models.DateTimeField(auto_now_add=True)         # Fecha y tiempo en el que se define un US
    asignado_a_sprint = models.BooleanField(default=False)
    valor_prioridad = models.FloatField(default=0.0) #Prioridad en numeros

    def __unicode__(self):
        return self.descripcion_corta

    def get_descripcion(self):
        return self.descripcion_larga

    def get_prioridad(self):
        return self.prioridad

    def get_prioridad_verbose(self):
        return dict(UserStory.prioridades_opciones)[self.prioridad]


    def get_developer(self,proyecto):

        us = UserStory.objects.get(id=self.id)
        pb = Product_Backlog.objects.get(proyecto=proyecto, us=us)
        if pb.usuario is None:
            return None
        return pb.usuario.user

    #funcion que recibe un usuario y un proyecto. Verifica si el usuario es developer de este us del proyecto
    def esDeveloper(self,user,proyecto):
        try:
            usuario = Usuario.objects.get(user=user)
            pb = Product_Backlog.objects.get(proyecto=proyecto, us=self)
            if pb.usuario == usuario:
                return True
        except ObjectDoesNotExist:
            return False
        return False

class Sprint(models.Model):
    duracion_dias = models.IntegerField(default=0)
    capacidad_horas = models.IntegerField(default=0)
    estado_opciones = (('PN', 'Planificado'), ('FI', 'Finalizado'), ('EJ', 'En Ejecucion'))
    estado = models.CharField(choices=estado_opciones,max_length=2,default='PN')
    avance_horas = models.IntegerField(default=0)
    horas_para_terminar = models.IntegerField(default=0)

    def get_estado_verbose(self):
        return dict(Sprint.estado_opciones)[self.estado]

    def is_US_Finalizados(self, idUs):
        listaActividades_us = Actividad_Us.objects.filter(us_id__in=idUs)

        for au in listaActividades_us:
            #verificar si estado es DN

            if 'DN' != au.estado:
                return False
            # comprobar si la actividad es la ultima en su flujo
            # obtener maximo orden del flujo en el que esta esta actividad
            max_orden = Actividad.objects.filter(flujo_id=au.actividad.flujo_id).aggregate(Max('orden'))['orden__max']
            # verificar si esta actividad tiene ese orden
            if au.actividad.orden != max_orden:
                return False
        return True

    def set_Priorizar_US(self, idUs,idP,idS):
        listaActividades_us = Actividad_Us.objects.filter(us_id__in=idUs)
        proyecto = Proyecto.objects.get(id=idP)
        sprint = Sprint.objects.get(id=idS)
        for au in listaActividades_us:
            # comprobar si la actividad es la ultima en su flujo
            # obtener maximo orden del flujo en el que esta esta actividad
            max_orden = Actividad.objects.filter(flujo_id=au.actividad.flujo_id).aggregate(Max('orden'))['orden__max']

            if (au.estado != 'DN') or (au.estado == 'DN' and au.actividad.orden != max_orden):
                au.us.prioridad = 'AL'
                au.us.asignado_a_sprint = False
                pb = Product_Backlog.objects.get(proyecto=proyecto,us=au.us)
                pb.usuario = None
                pb.save()
                psu = Proyecto_Sprint_Us.objects.get(proyecto=proyecto,sprint=sprint,us=au.us)
                psu.us = None
                psu.save()
                au.us.save()

    def puedeEjecutarse(self,proyecto):
        '''
            *Condiciones para iniciar Sprint:
            1. Que haya por lo menos un US.
            2. Cada US debe tener flujo.
            3. Cada US debe tener developer.
            4. Cada developer debe tener horas asignadas.
        '''
        # 1.
        lista_us = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto, sprint=self).exclude(us=None)

        # 2. y 3.
        todosUStienenFlujo = True
        todoUStieneDeveloper = True
        for elemento_lista in lista_us:
            if len(Flujo_Us.objects.filter(us=elemento_lista.us)) == 0:
                todosUStienenFlujo = False
            if todosUStienenFlujo:
                elemento_pb = Product_Backlog.objects.get(proyecto=proyecto, us=elemento_lista.us)
                if elemento_pb.usuario is None:
                    todoUStieneDeveloper = False

        # 4.
        cadaDeveloperTieneHorasAsignadas = True
        if len(
                Sprint_Developers.objects.filter(proyecto=proyecto, sprint=self, hs_trabajo_usuario=0)
        ) > 0:
            cadaDeveloperTieneHorasAsignadas = False

        if (len(lista_us) > 0 and todosUStienenFlujo
            and todoUStieneDeveloper
            and cadaDeveloperTieneHorasAsignadas):
            return True
        return False
    def getCondicionesParaEjecutarse(self,proyecto):
        '''
            *Condiciones para iniciar Sprint:
            1. Que haya por lo menos un US.
            2. Cada US debe tener flujo.
            3. Cada US debe tener developer.
            4. Cada developer debe tener horas asignadas.
        '''
        # 1.
        lista_us = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto, sprint=self).exclude(us=None)

        # 2. y 3.
        todosUStienenFlujo = True
        todoUStieneDeveloper = True
        for elemento_lista in lista_us:
            if len(Flujo_Us.objects.filter(us=elemento_lista.us)) == 0:
                todosUStienenFlujo = False
            if todosUStienenFlujo:
                elemento_pb = Product_Backlog.objects.get(proyecto=proyecto, us=elemento_lista.us)
                if elemento_pb.usuario is None:
                    todoUStieneDeveloper = False

        # 4.
        cadaDeveloperTieneHorasAsignadas = True
        if len(
                Sprint_Developers.objects.filter(proyecto=proyecto, sprint=self, hs_trabajo_usuario=0)
        ) > 0:
            cadaDeveloperTieneHorasAsignadas = False

        if (len(lista_us) > 0 and todosUStienenFlujo
            and todoUStieneDeveloper
            and cadaDeveloperTieneHorasAsignadas):
            puedeEjecutarse= True
        else:
            puedeEjecutarse=False

        lista={
            'puedeEjecutarse':puedeEjecutarse,
            'hayUS':len(lista_us) > 0,
            'todosUStienenFlujo':todosUStienenFlujo,
            'todoUStieneDeveloper':todoUStieneDeveloper,
            'cadaDeveloperTieneHorasAsignadas':cadaDeveloperTieneHorasAsignadas,

        }
        return lista
    def estaEnEjecucion(self):
        if self.estado == "EJ":
            return True
        return False

class Proyecto(models.Model):
    estados_opciones = (('IN', 'Iniciado'), ('FI', 'Finalizado'), ('CA', 'Cancelado'), ('EP', 'En Preparacion'))
    nombre_corto = models.CharField(max_length=20)
    nombre_largo = models.CharField(max_length=50)
    estado = models.CharField(choices=estados_opciones, max_length=2, default='EP')
    inicio = models.DateTimeField(auto_now_add=True)
    fin = models.DateTimeField(null=True)
    descripcion = models.TextField(max_length=500)
    lista_sprints = models.ManyToManyField(Sprint,through='Proyecto_Sprint_Us', through_fields=('proyecto','sprint','us'))
    product_backlog = models.ManyToManyField(UserStory,through='Product_Backlog', through_fields=('proyecto','us','usuario'))
    descripcion_cancelado = models.TextField(blank=True,null=True,max_length=500)

    def getListaPermisosDeUsuario(self,user):
        # obtener permisos de un usuario
        usuario = Usuario.objects.get(user=user)
        idPermisos = usuario.getListaPermisos(self).values_list('permiso_id', flat=True)
        permisosAsociados = Permiso.objects.filter(id__in=idPermisos).values_list('nombre', flat=True)
        return permisosAsociados
    def __unicode__(self):
        return self.nombre_corto

    def get_descripcion(self):
        return self.descripcion

    def get_estado(self):
        return self.estado

    def get_estado_verbose(self):
        return dict(Proyecto.estados_opciones)[self.estado]

    def get_Scrum(self):
        proyecto = Proyecto.objects.get(id=self.id)
        up = Usuario_Proyecto.objects.get(rol_id=1,proyecto=proyecto)
        return up.usuario.user

    def puede_finalizar(self):
        proyecto = Proyecto.objects.get(id=self.id)
        if proyecto.estado == 'IN':
            #Se crea una lista de los sprints del proyecto.
            sprint_terminados = True
            #Para que se pueda finalizar un proyecto todos los Sprints deben estar finalizados
            lista_sprint = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto)
            for x in lista_sprint:
                if x.sprint.estado != 'FI':
                    sprint_terminados = False
        else:
            sprint_terminados = False
        return sprint_terminados

class Usuario(models.Model):
    user = models.OneToOneField(User)
    direccion=models.TextField(null=True, blank=True)
    telefono=models.TextField(null=True, blank=True)
    proyectos = models.ManyToManyField(Proyecto,
                                       through='Usuario_Proyecto',
                                       through_fields=('usuario', 'proyecto'),
                                       )

    def getListaPermisos(self, proyecto):
        try:
            # Obtener registro que relaciona proyecto con usuario
            # listar id de todos los roles del usuario en el proyecto
            usuProy = Usuario_Proyecto.objects.filter(usuario=self, proyecto=proyecto).values_list('rol_id', flat=True)
            if len(usuProy) == 0:
                return None
            # Recuperar permisos asociados a todos sus roles
            permisos = Rol_Permiso.objects.filter(rol__id__in=usuProy)
            return permisos

        except ObjectDoesNotExist:
            return None

    def tienePermiso(self, proyecto, permisoRequerido):
        permisos = self.getListaPermisos(proyecto)
        if permisos is not None:
            # Comprobar permiso actual de usuario
            try:
                p = Permiso.objects.get(nombre=permisoRequerido)
                if len(permisos.filter(permiso=p).values_list('id',flat=True)) > 0:
                    return True
            except ObjectDoesNotExist:
                return False
        return False

        # funcion que recibe una lista de ID de User Stories y comprueba si estos han terminado de finalizar sus flujos
    def isUSfinalizados(self, idUs):
        #idUs = [1, 2]
        # lista de objetos Actividades
        listaActividades_us = Actividad_Us.objects.filter(us_id__in=idUs)
        idActividades=listaActividades_us.values_list('actividad_id', flat=True)
        listaActividades = Actividad.objects.order_by('id').filter(id__in=idActividades)

        idFlujos = listaActividades.values_list('flujo_id', flat=True)
        idActividades = listaActividades.values_list('id', flat=True)

        for au in listaActividades_us:
            #verificar si estado es DN

            if 'DN' != au.estado:
                return False
            # comprobar si la actividad es la ultima en su flujo
            # obtener maximo orden del flujo en el que esta esta actividad
            max_orden = Actividad.objects.filter(flujo_id=au.actividad.flujo_id).aggregate(Max('orden'))['orden__max']
            # verificar si esta actividad tiene ese orden
            if au.actividad.orden != max_orden:
                return False
        return True

    def __unicode__(self):
        return self.user.username

class Permiso(models.Model):
    nombre = models.TextField(max_length=50)

    def __unicode__(self):
        return self.nombre

class Rol(models.Model):
    nombre = models.CharField(max_length=50)
    permisos = models.ManyToManyField(Permiso,through='Rol_Permiso',through_fields=('rol', 'permiso'),)

    def __unicode__(self):
        return self.nombre

class Rol_Permiso(models.Model):
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    permiso = models.ForeignKey(Permiso, on_delete=models.CASCADE, default=None)

    def __unicode__(self):
        nombre = 'Rol: ' + self.rol.nombre + '| Permiso: ' + self.permiso.nombre
        return nombre

class Usuario_Proyecto(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, default=None)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, default=None)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE, default=None)

    def __unicode__(self):
        nombre = 'Proyecto: ' + self.proyecto.nombre_corto + '| Usuario: ' + self.usuario.user.username
        return nombre

class Flujo_Proyecto (models.Model):
    descripcion = models.TextField(null=True, blank=True)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, default=None)
    lista_us= models.ManyToManyField(UserStory,through='Flujo_Us',through_fields=('flujo', 'us'))

class Flujo_Us(models.Model):
    flujo = models.ForeignKey(Flujo_Proyecto, on_delete=models.CASCADE)
    us = models.OneToOneField(UserStory, on_delete=models.CASCADE, default=None)

class Actividad(models.Model):
    flujo= models.ForeignKey(Flujo_Proyecto, on_delete=models.CASCADE, default=None)
    orden= models.IntegerField(blank=False,null=False)
    descripcion = models.TextField(null=False, blank=False)


class Proyecto_Sprint_Us(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, default=None)
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, default=None)
    us= models.OneToOneField(UserStory, default=None, null=True, blank=True) #Si no funcion algo pasar a ForeignKey
    #usuario = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE, default=None)

class Product_Backlog(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, default=None, related_name='proyectoPadre')
    us = models.OneToOneField(UserStory, on_delete=models.CASCADE, default=None)
    usuario = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE, default=None)

class Actividad_Us(models.Model):
    actividad = models.ForeignKey(Actividad, on_delete=models.CASCADE, default=None)
    us = models.OneToOneField(UserStory, on_delete=models.CASCADE, default=None)
    estado_opciones = (('TD', 'To Do'), ('DO', 'Doing'), ('DN', 'Done'))
    estado = models.CharField(choices=estado_opciones, max_length=2, default='TD', blank=True, null=True)
    def get_estado(self):
        return self.estado

    def get_estado_verbose(self):
        return dict(Actividad_Us.estado_opciones)[self.estado]

class Sprint_Developers(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, default=None)
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, default=None)
    usuario = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE, default=None)
    hs_trabajo_usuario = models.IntegerField(default=0)

class Archivo_Us(models.Model):
    us = models.ForeignKey(UserStory, on_delete=models.CASCADE, default=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    archivo= models.TextField(null=True, blank=True)
    nombreArchivo=models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

class Nota_Us(models.Model):
    us = models.ForeignKey(UserStory, on_delete=models.CASCADE, default=None)
    user=models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    nota= models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

class Log_Us(models.Model):
    us = models.ForeignKey(UserStory, on_delete=models.CASCADE, default=None)
    descripcion= models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

class Log_Sprint(models.Model):
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, default=None)
    descripcion= models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

class Log_Flujo(models.Model):
    flujo = models.ForeignKey(Flujo_Proyecto, on_delete=models.CASCADE, default=None)
    descripcion= models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)