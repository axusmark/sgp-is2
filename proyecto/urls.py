#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from proyecto import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'proyecto'


urlpatterns = [
    url(r'^$',views.vista_index, name="vista_index"),
    #PROYECTO
    url(r'^crear_proyecto/$', views.vista_crear_proyecto, name="vista_crear_proyecto"),
    url(r'^(?P<idP>\d+)/consultar_proyecto$', views.vista_consultar_proyecto, name="vista_consultar_proyecto"),
    url(r'^listar_proyectos/$', views.vista_listar_proyectos, name="vista_listar_proyectos"),
    url(r'^(?P<idP>\d+)/modificar_proyecto/$', views.vista_modificar_proyecto, name="vista_modificar_proyecto"),
    url(r'^eliminar_proyecto/$', views.vista_eliminar_proyecto, name="vista_eliminar_proyecto"),
    url(r'^mis_proyectos/$', views.vista_mis_proyectos, name="vista_mis_proyectos"),
    url(r'^(?P<idP>\d+)/agregar_participantes/$', views.vista_agregar_participantes, name="vista_agregar_participantes"),
    url(r'^(?P<idP>\d+)/cancelar_proyecto/$', views.vista_cancelar_proyecto, name="vista_cancelar_proyecto"),
    url(r'^(?P<idP>\d+)/retomar_proyecto/$', views.vista_retomar_proyecto, name="vista_retomar_proyecto"),
    url(r'^(?P<idP>\d+)/finalizar_proyecto/$', views.vista_finalizar_proyecto, name="vista_finalizar_proyecto"),
    url(r'^(?P<idP>\d+)/modificar_SM/$', views.vista_modificar_SM, name="vista_modificar_SM"),

    #USUARIOS
    url(r'^(?P<getIdUsuario>[0-9]+)/ver_usuario/$', views.vista_verUsuario, name="vista_verUsuario"),
    url(r'^lista_usuarios', views.vista_listaUsuarios, name="vista_listaUsuarios"),
    url(r'^(?P<getIdUsuario>[0-9]+)/editar_usuario/$', views.vista_editarUsuario, name="vista_editarUsuario"),
    url(r'^nuevo_usuario/$', views.vista_nuevoUsuario, name="vista_nuevoUsuario"),
    url(r'^eliminar_usuario/$', views.vista_eliminarUsuario, name="vista_eliminarUsuario"),
    url(r'^(?P<getIdUsuario>[0-9]+)/cambiar_contrasenha/$', views.vista_cambiarContrasenha, name="vista_cambiarContrasenha"),

    #PERMISOS
    url(r'^lista_permisos', views.vista_listaPermisos, name="vista_listaPermisos"),
    url(r'^nuevo_permiso/$', views.vista_nuevoPermiso, name="vista_nuevoPermiso"),
    url(r'^(?P<getIdPermiso>[0-9]+)/ver_permiso/$', views.vista_verPermiso, name="vista_verPermiso"),
    url(r'^(?P<getIdPermiso>[0-9]+)/editar_permiso/$', views.vista_editarPermiso, name="vista_editarPermiso"),

    #ROLES
    url(r'^(?P<getIdRol>[0-9]+)/ver_rol/$', views.vista_verRol, name="vista_verRol"),
    url(r'^lista_roles', views.vista_listaRoles, name="vista_listaRoles"),
    url(r'^nuevo_rol/$', views.vista_nuevoRol, name="vista_nuevoRol"),
    url(r'^(?P<getIdRol>[0-9]+)/editar_rol/$', views.vista_editarRol, name="vista_editarRol"),

    #USER STORY
    url(r'^(?P<idP>[0-9]+)/nuevo_us/$', views.vista_crear_userstory, name="vista_crear_userstory"),
    url(r'^eliminar_us/$', views.vista_eliminarUS, name="vista_eliminarUS"),
    url(r'^(?P<idP>[0-9]+)/consultar_us/(?P<idUs>[0-9]+)/$', views.vista_consultar_us, name="vista_consultar_us"),
    url(r'^(?P<idP>[0-9]+)/lista_us/$', views.vista_lista_us, name="vista_lista_us"),
    url(r'^(?P<idP>[0-9]+)/modificar_us/(?P<idUs>[0-9]+)/$', views.vista_modificar_us, name="vista_modificar_us"),
    #url(r'^(?P<idP>[0-9]+)/agregar_developer_us/(?P<idUs>[0-9]+)/$', views.vista_agregar_developer_us, name="vista_agregar_developer_us"), se paso a la parte de Sprint
    url(r'^(?P<idP>[0-9]+)/avance_us/(?P<idUs>[0-9]+)/$', views.vista_registrar_avance_us, name="vista_registrar_avance_us"),
    url(r'^(?P<idP>[0-9]+)/subir_archivo/(?P<idUs>[0-9]+)/$', views.vista_subirArchivo, name="vista_subirArchivo"),
    url(r'^eliminar_archivo/$', views.vista_eliminarArchivo, name="vista_eliminarArchivo"),
    url(r'^eliminar_nota/$', views.vista_eliminarNota, name="vista_eliminarNota"),

    #formularios
    url(r'^eliminar_permiso/$', views.vista_eliminarPermiso, name="vista_eliminarPermiso"),
    url(r'^eliminar_rol/$', views.vista_eliminarRol, name="vista_eliminarRol"),

    #SPRINTS
    url(r'^(?P<idP>[0-9]+)/crear_sprint/$', views.vista_crear_sprint,name="vista_crear_sprint"),
    url(r'^(?P<idP>[0-9]+)/lista_sprint', views.vista_lista_sprint, name="vista_lista_sprint"),
    url(r'^(?P<idP>[0-9]+)/consultar_sprint/(?P<idS>[0-9]+)$', views.vista_consultar_sprint, name="vista_consultar_sprint"),
    url(r'^(?P<idP>[0-9]+)/modificar_sprint/(?P<idS>[0-9]+)$', views.vista_modificar_sprint, name="vista_modificar_sprint"),
    url(r'^eliminar_sprint/$', views.vista_eliminar_sprint, name="vista_eliminar_sprint"),
    url(r'^(?P<idP>[0-9]+)/agregar_us/(?P<idS>[0-9]+)$', views.vista_agregar_us, name="vista_agregar_us"),
    url(r'^(?P<idP>[0-9]+)/agregar_developers_sprint/(?P<idS>[0-9]+)$', views.vista_agregar_developer_sprint, name="vista_agregar_developer_sprint"),
    url(r'^(?P<idP>[0-9]+)/agregar_developer_us/(?P<idS>[0-9]+)/(?P<idUs>[0-9]+)/$', views.vista_agregar_developer_us, name="vista_agregar_developer_us"),
    url(r'^(?P<idP>[0-9]+)/agregar_horas_trabajo_sprint/(?P<idS>[0-9]+)$', views.vista_asignar_horas_trabajo, name="vista_asignar_horas_trabajo"),
    url(r'^(?P<idP>[0-9]+)/ejecutar_sprint/(?P<idS>[0-9]+)$', views.vista_ejecutar_sprint, name="vista_ejecutar_sprint"),
    url(r'^finalizar_sprint/$', views.vista_finalizar_sprint, name="vista_finalizar_sprint"),

    #flujos
    url(r'^(?P<idP>[0-9]+)/lista_flujos', views.vista_listaFlujos, name="vista_listaFlujos"),
    url(r'^(?P<idP>[0-9]+)/nuevo_flujo/$', views.vista_nuevoFlujo, name="vista_nuevoFlujo"),
    url(r'^ver_flujo/(?P<idP>[0-9]+)/(?P<idFlujo>[0-9]+)/$', views.vista_verFlujo, name="vista_verFlujo"),
    url(r'^editar_flujo/(?P<idP>[0-9]+)/(?P<idFlujo>[0-9]+)/$', views.vista_editarFlujo, name="vista_editarFlujo"),
    url(r'^editar_estadoUS/(?P<idP>[0-9]+)/(?P<idFlujo>[0-9]+)/(?P<idUs>[0-9]+)/$', views.vista_editarEstadoUS, name="vista_editarEstadoUS"),
    url(r'^editar_actividad/(?P<idP>[0-9]+)/(?P<idFlujo>[0-9]+)/(?P<idActividad>[0-9]+)/$', views.vista_editarActividad,name="vista_editarActividad"),
    url(r'^agregar_us_a_flujo/(?P<idP>[0-9]+)/(?P<idUS>[0-9]+)/(?P<idS>[0-9]+)$', views.vista_agregarUSaFlujo,name="vista_agregarUSaFlujo"),

    #kanban
    url(r'^lista_kanban/(?P<idP>[0-9]+)/$', views.vista_listaKanban, name="vista_listaKanban"),
    url(r'^ver_kanban/(?P<idP>[0-9]+)/(?P<idFlujo>[0-9]+)/$', views.vista_verKanban, name="vista_verKanban"),
]
