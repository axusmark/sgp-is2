#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User

#Formularios de Proyecto
class FormCrearProyecto(forms.Form):
    nombre_corto_form = forms.CharField(max_length=20)
    nombre_largo_form = forms.CharField(max_length=50)
    descripcion_form = forms.CharField()
    scrum_form = forms.CharField()

class FormAgregarParticipantes(forms.Form):
    owner_form = forms.CharField()

class FormModificarProyecto(forms.Form):
    nombre_corto_form = forms.CharField(max_length=20,)
    nombre_largo_form = forms.CharField(max_length=50,)
    descripcion_form = forms.CharField()
    fin_form = forms.DateTimeField(required=False)

#Formularios de Usuarios - Permisos - Roles
class FormEditarUsuario(forms.Form):
    username= forms.CharField()
    nombre=forms.CharField()
    apellido=forms.CharField()
    email=forms.CharField()

class FormEditarPermiso(forms.Form):
    nombre=forms.CharField()

class FormEditarRol(forms.Form):
    nombre=forms.CharField()

#Formularios de User Story
class FormCrearUserStory(forms.Form):
    descripcion_corta_form = forms.CharField(max_length=140)        # Breve descripcion del US
    descripcion_larga_form = forms.CharField(max_length=500)                   # Descripcion mas detallada del US
    tiempo_estimado_form = forms.FloatField()                       # Tiempo estimado que tomara desarrollar el sprint
    prioridad_form = forms.CharField()                              # Por default su prioridad sera media
    valor_de_negocios_form = forms.FloatField()   # El valor de negocios que tendra un US
    valor_tecnico_form = forms.FloatField()   # El valor tecnico de un US



class FormCrearFlujo(forms.Form):
    #estado = forms.CharField()
    wip = forms.FloatField(required=True)

class FormCrearActividad(forms.Form):
    idFlujo= forms.IntegerField( required=True)
    orden= forms.IntegerField(required=True)
    estado = forms.CharField(required=True)
    descripcion = forms.CharField(required=True)

class FormModificarUserStory(forms.Form):
    descripcion_corta_form = forms.CharField(max_length=140)
    descripcion_larga_form = forms.CharField(max_length=500)
    tiempo_estimado_form = forms.FloatField()
    prioridad_form = forms.CharField()
    valor_de_negocios_form = forms.IntegerField()
    valor_tecnico_form = forms.IntegerField()


class FormRegistarAvanceUs(forms.Form):
    tiempo_dedicado_form = forms.FloatField()

#Formularios de Sprints
class FormCrearSprint(forms.Form):
    duracion_dias_form = forms.IntegerField()

class FormModificarUserStory(forms.Form):
    descripcion_corta_form = forms.CharField(max_length=140)
    descripcion_larga_form = forms.CharField(max_length=500)
    tiempo_estimado_form = forms.FloatField()
    prioridad_form = forms.CharField()
    valor_de_negocios_form = forms.IntegerField()
    valor_tecnico_form = forms.IntegerField()


class FormRegistarAvanceUs(forms.Form):
    tiempo_dedicado_form = forms.FloatField()

#Formularios de Sprints
class FormCrearSprint(forms.Form):
    duracion_dias_form = forms.IntegerField()

class UploadFileForm(forms.Form):
    titulo = forms.CharField(max_length=50)
    archivo = forms.FileField()