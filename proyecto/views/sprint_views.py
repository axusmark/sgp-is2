#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import messages
from django.shortcuts import render
from proyecto.forms import *
from proyecto.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import redirect
import datetime


@login_required
@csrf_protect
def vista_crear_sprint(request,idP):
    """
    Esta funcion vista permite crear un nuevo Sprint asociado a un proyecto
    @param request: parametro recibido por defecto por las vistas de Django.
    @param idP: Identificador unico del proyecto
    @return: Se renderiza un template con formularios para la creacion de Sprints
    """
    msj_error = ''
    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'crear_sprint'):
        return HttpResponseRedirect('/')

    if request.method=='POST':
        form = FormCrearSprint(request.POST)
        if form.is_valid():
            datos = form.cleaned_data
            sprint = Sprint()
            sprint.duracion_dias = datos['duracion_dias_form']
            sprint.estado = 'PN'
            sprint.save()
            #Se relaciona sprint con proyecto en la tabla intermedia
            psu = Proyecto_Sprint_Us.objects.create(proyecto=proyecto,sprint=sprint)
            #Se relaciona sprint con developer en la tabla intermedia
            psu = Sprint_Developers.objects.create(proyecto=proyecto,sprint=sprint)
            psu.save()
            #Registrar en el log
            Log_Sprint.objects.create(sprint=sprint,descripcion="Se ha creado el sprint.")
            # Mensaje de que se ha creado un US con exito
            messages.success(request,"Se creo exitosamente el Sprint")
            return HttpResponseRedirect(reverse('proyecto:vista_lista_sprint', args=[idP]))
        else:
            msj_error = 'Ten en cuenta las indicaciones sobre los campos'
    else:
        # Si no se cargaron los datos se devuelve un formulario vacio
        form = FormCrearUserStory

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'form':form,
        'proyecto':proyecto,
        'idP':idP,
        'msj_error':msj_error,
        'permisosAsociados':permisosAsociados,
    }

    return render(request,"html/sprints/crear_sprint.html",context)

@login_required
def vista_lista_sprint(request,idP):
    """
    La funcion vista "Lista Sprints" presenta un listado de todos los sprints registrados
    @param request: Parametro por defecto que recibe una vista de Django.
    @param idP: Identificador unico del Proyecto
    @return: Renderiza el html correspondiente, el cual recibe por contexto la lista de sprints
    """

    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'listar_sprint'):
        return HttpResponseRedirect('/')

    #Lista de Sprints asociadas al proyecto
    lista_sprints = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto).all().distinct('sprint_id')
    listaSprints=[]
    for i in lista_sprints:
        listaSprints.append([i,i.sprint.puedeEjecutarse(proyecto)])

    #Bandera para saber si hay varios en ejecución
    sprint_en_ejecucion = False
    for s in lista_sprints:
        if s.sprint.estado == 'EJ':
            sprint_en_ejecucion = True

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)
    #if not (usuario.tienePermiso(proyecto, 'agregar_participantes') and
                #usuario.tienePermiso(proyecto, 'crear_us')):


    context = {
        'lista_sprints': lista_sprints,
        'listaSprints':listaSprints,
        'idP':idP,
        'proyecto':proyecto,
        'permisosAsociados':permisosAsociados,
        'sprint_en_ejecucion':sprint_en_ejecucion,
    }
    return render(request, "html/sprints/lista_sprint.html", context)

@login_required
def vista_consultar_sprint(request, idP, idS):
    """
    La funcion "Consultar Sprint" nos permite ver los detalles de un Sprint especifico.
    @param request:parametro por defecto que recibe una vista de Django
    @param idP: Identificador unico del Proyecto
    @param idS: Identificador unico del Sprint asociado al proyecto
    @return: se renderiza un template que muestra los detalles de un Sprint
    """
    #Solo se permite el acceso a los usuarios que tienen asociados el permiso de "Listar Sprints"
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'ver_sprint'):
        return HttpResponseRedirect('/')

    sprint = get_object_or_404(Sprint,id=idS)
    #Lista de User Stories del Sprint
    try:
        lista_us = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto,sprint=sprint).all().order_by('-us__valor_prioridad')
        #obtener los us asignados a flujos
        listaIdUsEstanEnFlujo=Flujo_Us.objects.all().values_list('us_id',flat=True)
    except Proyecto_Sprint_Us.DoesNotExist, Flujo_Us.DoesNotExist:
        lista_us = None
        listaIdUsEstanEnFlujo = None

    #Se crea un diccionario con todos los US del Sprint para instanciar los autores de los US
    dicc_US_dev = {}
    for x in lista_us:
        if x.us != None:
            us = UserStory.objects.get(id=x.us.id)
            pb = Product_Backlog.objects.get(proyecto=proyecto,us=us)
            dicc_US_dev[x.us.id] = pb.usuario

    #Se crea un diccionario con todos los US del Sprint para instanciar los flujos de los US
    dicc_US_flujos = {}
    for x in lista_us:
        if x.us != None:
            us = UserStory.objects.get(id=x.us.id)
            try:
                fus = Flujo_Us.objects.get(us=us)
            except Flujo_Us.DoesNotExist:
                fus = None
            if fus != None:
                dicc_US_flujos[x.us.id] = fus.flujo.descripcion
            else:
                dicc_US_flujos[x.us.id] = "None"


    #Lista de developers asociados al proyecto
    try:
        lista_developers_sin_repetir = Sprint_Developers.objects.filter(proyecto=proyecto,sprint=sprint).all()
    except Sprint_Developers:
        lista_developers_sin_repetir = None

    #Lista developers True o False, de acuerdo si existen o no Developers en el sprint
    ld_vacia = True
    for y in lista_developers_sin_repetir:
        if y.usuario != None:
            ld_vacia = False

    #Capacidad del Sprint
    capacidad = 0
    for x in lista_developers_sin_repetir:
        capacidad = x.hs_trabajo_usuario*sprint.duracion_dias + capacidad
    sprint.capacidad_horas = capacidad
    sprint.save()

    #Avance del Sprint
    avance_sprint = 0.0
    total_hs_us = 0
    for x in lista_us:
        if not x.us == None:
            avance_sprint = avance_sprint + int(x.us.tiempo_dedicado)
            total_hs_us = x.us.tiempo_estimado + total_hs_us
    sprint.avance_horas = avance_sprint
    #Horas estimadas para terminar Sprint
    sprint.horas_para_terminar = total_hs_us - sprint.avance_horas
    sprint.save()

    #Saldo Sprint
    horas_US = 0
    saldo = sprint.capacidad_horas - total_hs_us

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    #controlar las condiciones para ejecutarse el sprint
    condicionesEjecucion= sprint.getCondicionesParaEjecutarse(proyecto)
    mensaje = []
    if not condicionesEjecucion.get('puedeEjecutarse'):

        mensaje.append("Este sprint aún no puede ejecutarse porque:")
        if not condicionesEjecucion.get('hayUS'):
            mensaje.append(" ** No hay User Stories asignados a este sprint.")
        if not condicionesEjecucion.get('todosUStienenFlujo'):
            mensaje.append("** No todos los User Stories de este sprint estan asignados a algun flujo.")
        if not condicionesEjecucion.get('todoUStieneDeveloper'):
            mensaje.append("** No todos los User Stories tienen asignados un developer.")
        if not condicionesEjecucion.get('cadaDeveloperTieneHorasAsignadas'):
            mensaje.append("** No todos los developers de este sprint tienen horas de trabajo asignadas.")

    if total_hs_us > sprint.capacidad_horas:
        mensaje.append("OBS: La capacidad del Sprint es inferior al tiempo estimado para realizar los US del Sprint")

    lista_log=Log_Sprint.objects.order_by('id').filter(sprint=sprint)

    context = {
        'proyecto':proyecto,
        'sprint':sprint,
        'idP':idP,
        'idS':idS,
        'permisosAsociados':permisosAsociados,
        'lista_us':lista_us,
        'lista_developers_sin_repetir':lista_developers_sin_repetir,
        'mensaje':mensaje,
        'ld_vacia':ld_vacia,
        'listaIdUsEstanEnFlujo':listaIdUsEstanEnFlujo,
        'lista_log':lista_log,
        'dicc_US_dev':dicc_US_dev,
        'dicc_US_flujos':dicc_US_flujos,
        'saldo':saldo,
        'total_hs_us':total_hs_us,
    }

    return render(request, "html/sprints/consultar_sprint.html", context)

@login_required
def vista_modificar_sprint(request, idP, idS):
    """
    La funcion vista "Editar Sprint" presenta un formulario para modificar los datos actuales de un sprint
    cuyo ID se recibe por parámetro.
    @param request:
    @param idP:
    @return: Renderiza el html encargado de presentar los detalles del sprint, con una alerta de confirmación de
    persistencia.
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'editar_sprint'):
        return HttpResponseRedirect('/')

    #Se instancia el sprint con los datos actuales
    sprint = get_object_or_404(Sprint,id=idS)
    #Si se envian datos, entonces se ingresa en el ciclo y se actualizan los nuevos datos
    if request.method == 'POST':
        form = FormCrearSprint(request.POST)
        #Solamente se realiza si el formulario es valido
        if form.is_valid():
            datos = form.cleaned_data
            sprint.duracion_dias = datos['duracion_dias_form']
            sprint.save()
            Log_Sprint.objects.create(sprint=sprint,descripcion="Se ha modificado el sprint.")
            messages.success(request,"Se modificó exitosamente el Sprint")
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_sprint', args=[idP,idS]))
    else:
        form = FormCrearSprint()

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'form':form,
        'proyecto':proyecto,
        'sprint':sprint,
        'idP':idP,
        'idS':idS,
        'permisosAsociados':permisosAsociados,
    }
    return render(request, "html/sprints/modificar_sprint.html", context)

@login_required()
def vista_eliminar_sprint(request):
    """
        La funcion vista "Eliminar Sprint" elimina el sprint cuyo ID se recibe por POST
        @param request: parametro recibido por defecto en las vistas de Django.
        @return: Redirecciona a la vista "Lista Sprints".
        """
    if(request.method=='POST'):
        idP = request.POST.get('idP')
        idS = request.POST.get('idS')

        #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
        proyecto = Proyecto.objects.get(id=idP)
        usuario= Usuario.objects.get(user=request.user)
        if not usuario.tienePermiso(proyecto,'eliminar_sprint'):
            return HttpResponseRedirect('/')

        sprint = Sprint.objects.get(id=idS)
        #Poner los US disponibles para otros Sprints
        lista_us = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto,sprint=sprint)
        for x in lista_us:
            if x.us!=None:
                x.us.asignado_a_sprint = False
                x.us.save()

        sprint.delete()
        response = {}
        return JsonResponse(response)
    return HttpResponseRedirect('/')

@login_required()
def vista_agregar_us(request, idP, idS):
    """
    La funcion permite agregar US a un Sprint seleccionado
    @param requets: Parametro recibido por defecto en las vistas de Django.
    @param idP: Identificador unico del Proyecto
    @param idS: Identificador unico del Sprint
    @return: Se renderiza un template que muestra un formulario para agregar US al Sprint
    """

    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'agregar_us_a_sprint'):
        return HttpResponseRedirect('/')

    #Sprint instanciado
    sprint = get_object_or_404(Sprint,id=idS)
    #Se intenta instanciar la lista de todos los US del Proyecto
    try:
        pb = Product_Backlog.objects.filter(proyecto=idP).all()
    except Product_Backlog.DoesNotExist:
        pb = None

    #Se ingresa al ciclo una vez que el formulario es completado, en la primera llamada a la funcion no se ingresa
    if request.method == "POST":
        #Borrar los US actuales asociados al Proyecto y al Sprint antes de asignar los nuevos
        us_actuales_en_sprint = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto,sprint=sprint).all()
        for x in us_actuales_en_sprint:
            if x.us != None:
                x.us.asignado_a_sprint = False
                x.us.save()
            x.delete()

        #Obtener particpantes de checkboxes chequeados, procesar y guardar
        lista_sprints_nuevos = request.POST.getlist('us_form')
        #Se asignan todos los US seleccionados al proyecto y al sprint
        for idUsNuevo in lista_sprints_nuevos:
            us_nuevo = get_object_or_404(UserStory,id=idUsNuevo)
            try:
                us_en_sprint = Proyecto_Sprint_Us.objects.create(proyecto=proyecto,sprint=sprint,us=us_nuevo)
                us_en_sprint.save()
                # Registrar en el log
                Log_Us.objects.create(us=us_nuevo, descripcion="Se ha agregado al sprint: " + str(sprint.id))
                Log_Sprint.objects.create(sprint=sprint, descripcion="Se ha agregado el User Story: " + us_nuevo.descripcion_corta)
                #Se marca el US para no volver a mostrarse en una lista
                us_nuevo.asignado_a_sprint = True
                us_nuevo.save()
            except Proyecto_Sprint_Us.DoesNotExist:
                us_en_sprint = None
        """
        #Ya que por fin tenemos US en el sprint, vamos a cambiar el estado del mismo a "Iniciado"
        if sprint.estado =='PN':
            sprint.estado='EJ'
            sprint.save()
            messages.success(request,'Se cambio el estado del Sprint a Iniciado')
        """



        messages.success(request,'Se agregaron correctamente los US al sprint')
        return HttpResponseRedirect(reverse('proyecto:vista_consultar_sprint', args=[idP,idS]))
    else:
        #Seleccionamos solo los US que pertenecer al Sprint actual
        us_actuales_en_sprint = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto,sprint=sprint).all().exclude(us=None).order_by('-us__valor_prioridad')
        us_actuales = []
        horas_US = 0
        for i in us_actuales_en_sprint:
            us_actuales.append(i.us)
            horas_US = horas_US + i.us.tiempo_estimado

        #Calcular Saldo del Sprint
        saldo = sprint.capacidad_horas - horas_US

        #Seleccionamos el resto de los US en el proyecto
        us_en_proyecto = Product_Backlog.objects.filter(proyecto=proyecto).all().order_by('-us__valor_prioridad')
        #Seleccionamos los US que todavia no fueron asignados a otro SPRINT
        us_sin_asignar = []
        for x in us_en_proyecto:
            if x.us.asignado_a_sprint == False:
                us_sin_asignar.append(x.us)

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context={
        'idP':idP,
        'idS':idS,
        'pb':pb,
        'proyecto':proyecto,
        'permisosAsociados':permisosAsociados,
        'sprint':sprint,
        'us_sin_asingar':us_sin_asignar,
        'us_actuales':us_actuales,
        'saldo':saldo,
    }


    return render(request, "html/sprints/agregar_us.html",context)

@login_required()
def vista_agregar_developer_sprint(request, idP,idS):
    """
    Esta funcion asocia en una tabla intermedia a developers con un sprint.
    @param requets: Parametro recibido por defecto en las vistas de Django.
    @param idP: Identificador unico del Proyecto
    @param idS: Identificador unico del Sprint
    @return: Se renderiza un template que muestra un formulario para agregar Developers al Sprint
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'editar_sprint'):
        return HttpResponseRedirect('/')

    #Se instancia el objeto de la tabla intermedia que contiene el US, Usuario y Proyecto
    sprint = get_object_or_404(Sprint,id=idS)
    #Instanciamos el rol developer si existe
    developer = get_object_or_404(Rol,nombre='Developer')
    #Seleccionamos solo los usuarios que son developers en el proyecto
    lista_developers_proyecto = Usuario_Proyecto.objects.filter(proyecto=proyecto,rol=developer).all()
    #Cargamos los nombres de los developers en una lista de strings

    if request.method=='POST':

        #Obtener una lista de US que forman parte de nuestro proyecto
        l1 = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto,sprint=sprint).all()
        l2 = Product_Backlog.objects.filter(proyecto=proyecto).all()
        #Lista que contiene los US que forman parte del Sprint, en el proyecto
        l3 = []
        for j in l2:
            for k in l1:
                if j.us == k.us:
                    l3.append(j)

        #Instanciamos los developers actuales del sprint
        lista_developers_sprint = Sprint_Developers.objects.filter(proyecto=proyecto,sprint=sprint).all()

        #Obtenemos los developers marcados en el form
        participante_sprint = request.POST.getlist('participadentes_form')

        #Se crea una lista de los participantes developers checkeados en el form
        par = []
        for participante in participante_sprint:
            user = get_object_or_404(User,username= participante)
            usuario_sprint = get_object_or_404(Usuario,user=user)
            par.append(usuario_sprint)

        #Agregamos los nuevos developers que se marcaron en el form y que no existen actualmente en la BD
        for x in par:
            if not x.id in lista_developers_sprint.values_list('usuario_id',flat=True):
                developer_sprint = Sprint_Developers.objects.create(usuario=x,proyecto=proyecto,sprint=sprint)
                developer_sprint.save()
                usuario=Usuario.objects.get(user_id=x.id)
                user=User.objects.get(id=usuario.id)
                Log_Sprint.objects.create(sprint=sprint,descripcion="Se ha agregado al developer: "+ user.username)

        #Eliminamos los developers que actualmente estan en la BD pero que no fueron marcados en el form
        for x in lista_developers_sprint:
            if not x.usuario in par:
                for z in l3:
                    if x.usuario == z.usuario: #Para poner en None el developer de un US si se lo quitó del Sprint
                        z.usuario = None
                        z.save()
                #Registrar en el log
                try:
                    usuario = Usuario.objects.get(user_id=x.usuario.id)
                    user = User.objects.get(id=usuario.id)
                    Log_Sprint.objects.create(sprint=sprint, descripcion="Se ha quitado al developer: " + user.username )
                except AttributeError:
                    print None
                x.delete()

        #Ir a lista de Sprints

        messages.success(request,"Se agregaron correctamente los Developers al Sprint")
        return HttpResponseRedirect(reverse('proyecto:vista_lista_sprint', args=[idP]))
    else:
        #Lista de developers marcados actualmente
        lista_aux = Sprint_Developers.objects.filter(proyecto=proyecto,sprint=sprint).all()
        lista_developers_check = []

        for x in lista_aux:
            if x.usuario != None:
                lista_developers_check.append(x.usuario)

        #Lista de developers no marcados
        lista_developers_no_check = []
        for x in lista_developers_proyecto:
            if not x.usuario in lista_developers_check:
                lista_developers_no_check.append(x.usuario)

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'proyecto':proyecto,
        'lista_developers_check':lista_developers_check,
        'lista_developers_no_check':lista_developers_no_check,
        'permisosAsociados':permisosAsociados,
        'idP':idP,
        'idUs':idS,
    }

    return render(request,"html/sprints/agregar_developers_sprint.html",context)

@login_required()
@csrf_protect
def vista_agregar_developer_us(request,idP,idS,idUs):
    """
    La funcion permite relacionar a un Developer con un User Story.
    @param request: parametro por defecto de las vistas de Django
    @param idP: Identificador unico del proyecto
    @param idUs: Identificador unico del User Story asociado al proyecto
    @return: Se renderiza un template para que el usuario pueda seleccionar un developer para el User Story.
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles modificar user stories
    proyecto = get_object_or_404(Proyecto,id=idP)
    usuario= get_object_or_404(Usuario,user=request.user)
    if not usuario.tienePermiso(proyecto,'agregar_developer_us'):
        return HttpResponseRedirect('/')

    #Se instancia el objeto de la tabla intermedia que contiene el US, Usuario y Proyecto
    sprint = get_object_or_404(Sprint,id=idS)
    us = get_object_or_404(UserStory,id=idUs)
    pb = get_object_or_404(Product_Backlog,proyecto=proyecto,us=us)
    #Seleccionamos solo los usuarios que son developers en el proyecto y estan en el sprint que indicamos
    lista_developers_proyecto = Sprint_Developers.objects.filter(proyecto=proyecto,sprint=sprint).all()
    #Cargamos los nombres de los developers en una lista de strings
    if request.method=='POST':
        user = get_object_or_404(User,username=request.POST.get('idNombre_Developer'))
        developer_elegido = get_object_or_404(Usuario,user=user)
        pb.usuario = developer_elegido
        pb.save()
        # Registrar en el log
        Log_Sprint.objects.create(sprint=sprint, descripcion="Se ha agregado el developer '"+user.username+"' al User Story: "+us.descripcion_corta+".")
        Log_Us.objects.create(us=us, descripcion="Se ha asignado al developer '"+user.username+"'")

        messages.success(request,"Se agrego correctamente el Developer al User Story")
        return HttpResponseRedirect(reverse('proyecto:vista_consultar_sprint', args=[idP,idS]))

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'proyecto':proyecto,
        'pb':pb,
        'lista_developers_proyecto':lista_developers_proyecto,
        'permisosAsociados':permisosAsociados,
        'idP':idP,
        'idUs':idUs,
        'idS':idS,
    }

    return render(request,"html/us/agregar_desarrollador_us.html",context)

@login_required()
def vista_asignar_horas_trabajo(request,idP,idS):
    """
    La función permite asignar la cantidad de horas que va a trabajar un Developer en el sprint
    @param request: parametro por defecto de las vistas de Django
    @param idP: Identificador unico del proyecto
    @param idS: Identificador unico del Sprint asociado al Proyecto
    @return: Se renderiza un template para que el usuario pueda agregar horas de trabajo en el sprint
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'editar_sprint'):
        return HttpResponseRedirect('/')
    #Se instancia el sprint que tenemos como objetivo
    sprint = Sprint.objects.get(id=idS)
    #Se crea una lista que contiene todos los developers de nuestro actual sprint
    sd = Sprint_Developers.objects.filter(proyecto=proyecto,sprint=sprint).all()

    #Procesamos la cantidad de horas que va a trabajar cada Developer por día
    if request.method == 'POST':
        lista = request.POST.getlist('trabajo_horas_form')
        cont = 0
        for x in sd:
            x.hs_trabajo_usuario = lista[cont]
            cont = cont + 1
            x.save()

        # Registrar en el log
        Log_Sprint.objects.create(sprint=sprint, descripcion="Se han asignado horas de trabajo a los developers.")
        #Ir a lista de Sprints
        messages.success(request,"Se agregaron correctamente las horas de trabajo a los Developers del Sprint")
        return HttpResponseRedirect(reverse('proyecto:vista_consultar_sprint', args=[idP,idS]))

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'proyecto':proyecto,
        'idP':idP,
        'idS':idS,
        'sd':sd,
        'permisosAsociados':permisosAsociados,
    }
    return render(request,"html/sprints/asignar_horas_trabajo.html",context)

@login_required()
def vista_ejecutar_sprint(request,idP,idS):
    """
    Cambiar el estado de un Sprint a "En Ejecución"
    @param request: parametro por defecto de las vistas de Django
    @param idP: Identificador unico del proyecto
    @param idS: Identificador unico del Sprint asociado al Proyecto
    @return: Se renderiza al template de "Consultar Sprint"
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'ejecutar_sprint'):
        return HttpResponseRedirect('/')
    #Se instancia el sprint que tenemos como objetivo
    sprint = Sprint.objects.get(id=idS)
    #Se cambia el estado del Sprint a "En Ejecución" si previamente estaba en "Planificado"
    if sprint.estado == 'PN' and sprint.puedeEjecutarse(proyecto):
        sprint.estado = 'EJ'
        sprint.save()
        # Registrar en el log
        Log_Sprint.objects.create(sprint=sprint, descripcion="Se inicia la ejecucion")
        messages.success(request,"El Sprint se empezó a ejecutar")
    else:
        # Registrar en el log
        messages.warning(request, "El Sprint no cumple las condiciones necesarias para ejecutarse")
    return HttpResponseRedirect(reverse('proyecto:vista_consultar_sprint', args=[idP,idS]))

@login_required()
def vista_finalizar_sprint(request):
    """
        La funcion vista "Finalizar Sprint" cambia el estado del Sprint a Finalizado.
        @param request: parametro recibido por defecto en las vistas de Django.
        @return: Redirecciona a la vista "Lista Sprints".
        """
    if(request.method=='POST'):
        idP = request.POST.get('idP')
        idS = request.POST.get('idS')

        #Solo se permite el acceso a las personas que tienen asociados a sus roles crear sprints
        proyecto = Proyecto.objects.get(id=idP)
        usuario= Usuario.objects.get(user=request.user)
        if not usuario.tienePermiso(proyecto,'finalizar_sprint'):
            return HttpResponseRedirect('/')

        sprint = Sprint.objects.get(id=idS)
        #Lista de todos los US que forman parte del Sprint y del Proyecto
        ld = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto,sprint=sprint).values_list('us_id',flat=True)

        #obtener permisos de un usuario
        usuario = Usuario.objects.get(user=request.user)
        idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
        permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

        context = {
            'sprint':sprint,
            'ld':ld,
            'proyecto':proyecto,
            'permisosAsociados':permisosAsociados,
        }

        #Se cambia el estado del Sprint a Finalizado solo si estaba previamente "En Ejecución"
        if sprint.estado == 'EJ' and sprint.is_US_Finalizados(ld):
            sprint.estado = 'FI'
            sprint.save()
            messages.success(request,"Se finalizo correctamente el Sprint")
        else:
            sprint.set_Priorizar_US(ld,idP,idS)
            sprint.estado = 'FI'
            sprint.save()
            messages.success(request,"Los US que no se terminaron pasan con prioridad alta en el Product Backlog")
        #Registrar en el log
        Log_Sprint.objects.create(sprint=sprint,descripcion="Se ha finalizado el sprint.")
        response = {}
        return JsonResponse(response)
    return HttpResponseRedirect('/')