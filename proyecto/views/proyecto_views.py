#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import messages
from django.shortcuts import render
from proyecto.forms import *
from proyecto.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import redirect
import datetime

# Create your views here.
@login_required()
@csrf_protect
def vista_crear_proyecto(request):
    """
    La funcion vista "Crear Proyecto" crea un nuevo proyecto asignando al mismo un scrum master, y completando las tablas
    que estan relacionadas a los usuarios.. de manera a tener un registro de quienes son los Scrum's de los proyectos.
    @param request:
    @return: Renderiza un html que contiene un formulario para crear proyectos.
    """
    #Limita el acceso de esta vista solo al administrador del sistema
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    #Se listan los usuarios existentes en la base de datos, ordenados por su 'username'.
    lista_usuarios = User.objects.order_by('username').all().exclude(username='admin')
    lista_nombres = []
    #lista_usuarios es una lista de objetos User, por lo tanto solamente copiamos su campo 'username' en una lista de Strings
    for nombre_usuarios in lista_usuarios:
        lista_nombres.append(nombre_usuarios.username)
    #Cuando se renderiza por primera vez la pagina el request.method es igual a Null porque no se tratan de enviar datos.
    #Se ingresa en el ciclo if cuando se completa el formulario correspondiente y se envian los datos
    if request.method=="POST":
        form = FormCrearProyecto(request.POST)
        #Se pregunta si el formulario es valido (si los datos y campos estan bien rellenados)
        if form.is_valid():
            #Django se encarga de limpiar los datos del POST de acuerdo a lo especificado en nuestro formulario
            datos = form.cleaned_data
            #Se crea un nuevo proyecto, se le asgina los valores correspondientes
            project = Proyecto()
            project.nombre_corto = datos['nombre_corto_form']
            project.nombre_largo = datos['nombre_largo_form']
            project.descripcion = datos['descripcion_form']
            project.save()
            #Se recupera el username del scrum master elegido para el proyecto
            username_scrum = datos['scrum_form']
            try:
                #Se crea el usuario
                user_scrum = User.objects.get(username=username_scrum)
                usuario = Usuario.objects.get(user=user_scrum)
                #Se selecciona el rol de Scrum Master
                scrum = Rol.objects.get(nombre='Scrum Master')
                #Se completa la relacion en la tabla Usuario_Proyecto_Rol
                scrum_del_proyecto = Usuario_Proyecto.objects.create(proyecto=project, rol= scrum, usuario=usuario)
                scrum_del_proyecto.save()
                #Añadir al administrador de la plataforma, con rol de Administrador
                rolAdmin = Rol.objects.get(nombre='Administrador')
                usuarioAdmin=Usuario.objects.get(user=request.user)
                admin = Usuario_Proyecto.objects.create(proyecto=project, rol=rolAdmin, usuario=usuarioAdmin)
                admin.save()
            except Usuario_Proyecto.DoesNotExist, Usuario.DoesNotExist:
                usuario = None
                scrum_del_proyecto = None
            #Se carga el mensaje que se va a mostrar en la pagina donde se redirige
            messages.success(request,"Se creo exitosamente el proyecto")
            return HttpResponseRedirect(reverse('proyecto:vista_listar_proyectos'))
    else:
        #Si no se enviaron datos entonces se devuelve un formulario vacio
        form = FormCrearProyecto()

    context = {
        "form":form,
        'lista_usuarios':lista_nombres,
    }

    return render(request, "proyecto/crear_proyecto.html", context)

@login_required()
def vista_consultar_proyecto(request,idP):
    """
    La funcion vista "Consultar Proyecto" muestra detalles especificos de un proyecto seleccionado.
    @param request:
    @param idP: Identificador unico del proyecto seleccionado
    @return: Renderiza un html que contiene detalles del proyecto.
    """
    #Solamente se da acceso a los detalles a las personas que participan de alguna manera en el proyecto o
    #al administrador del sistema
    proyecto = get_object_or_404(Proyecto,id=idP)
    usuario_participante = get_object_or_404(Usuario,user=request.user)
    try:
        usuario_en_proyecto = Usuario_Proyecto.objects.filter(usuario=usuario_participante, proyecto=proyecto).all()
    except Usuario_Proyecto.DoesNotExist:
        usuario_en_proyecto = None

    if not usuario_en_proyecto and not request.user.is_staff:
        return HttpResponseRedirect('/')

    #Se busca el proyecto en la base de datos a travez de la ID recibida
    #Se buscan los roles en la base datos
    try:
        scrum = Rol.objects.get(nombre='Scrum Master')
        developer = Rol.objects.get(nombre='Developer')
        powner = Rol.objects.get(nombre='Product Owner')
    except Rol.DoesNotExist:
        scrum = None
        developer = None
        powner = None

    try:
        #proyecto = Proyecto.objects.get(id=idP)
        #Se busca en la tabla intermedia el Usuario que es Scrum Master del Proyecto especificado
        up_scrum = Usuario_Proyecto.objects.filter(rol=scrum,proyecto=proyecto)
    except Proyecto.DoesNotExist, Usuario_Proyecto.DoesNotExist:
        up_scrum = None
        proyecto = None

    #Se busca en la tabla intermedia el Usuario que es Product Owner del Proyecto especificado
    try:
        up_po = Usuario_Proyecto.objects.get(rol=powner,proyecto=proyecto)
    except Usuario_Proyecto.DoesNotExist:
        up_po = None

    #Se busca en la tabla intermedia los Usuarios que son Developers en Proyecto especificado
    try:
        up_developer = Usuario_Proyecto.objects.filter(rol=developer,proyecto=proyecto).all()
    except Usuario_Proyecto.DoesNotExist:
        up_developer = None

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)
    #if not (usuario.tienePermiso(proyecto, 'agregar_participantes') and
                #usuario.tienePermiso(proyecto, 'crear_us')):

    context= {
        'proyecto':proyecto,
        'up_scrum':up_scrum,
        'up_po':up_po,
        'up_developer':up_developer,
        'permisosAsociados': permisosAsociados,
        'idP':idP,
    }

    return render(request, "proyecto/consultar_proyecto.html", context)

@login_required()
def vista_listar_proyectos(request):
    """
    La funcion vista "Listar Proyectos" muestra todos los proyectos que se encuentran en la base de datos.
    @param request:
    @return: Renderiza un html con todos los proyectos en la base de datos
    """
    #Limita el acceso de esta vista solo al administrador del sistema
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    #Se crea una lista de proyectos
    lista_proyectos = Proyecto.objects.order_by('nombre_largo').all()
    try:
        #Se instancia al rol de Scrum Master
        scrum = Rol.objects.get(nombre='Scrum Master')
    except Rol.DoesNotExist:
        scrum = None
    #Se va consultando por cada proyecto cual es el Scrum Master del mismo.
    lista_proyectos_scrum = []
    for proyecto in lista_proyectos:
        #Por cada proyecto en lista_proyectos se pregunta el Scrum Master del mismo consultando en la tabla usuario_proyecto
        try:
            up = Usuario_Proyecto.objects.get(rol=scrum,proyecto=proyecto)
            #Se guardan los nombres de los Scrums Master en la lista
            lista_proyectos_scrum.append(up.usuario.user.username)
        except Usuario_Proyecto.DoesNotExist:
            up = None


    context = {
        'lista_proyectos':lista_proyectos,
        'lista_proyectos_scrum':lista_proyectos_scrum,
    }
    return render(request, "proyecto/listar_proyectos.html", context)

@login_required()
@csrf_protect
def vista_modificar_proyecto(request,idP):
    """
    La funcion vista "Modificar Proyecto" muestra un formulario que da la opcion de modificar atributos
    del objeto "Proyecto" y guardar los cambios en la base de datos.
    @param request:
    @param idP: Identificador del Proyecto que se va a modificar
    @return: Renderiza un html que contiene un formulario para modificar el proyecto seleccionado
    """
    #Se instancia el proyecto original sin modificar para que algunos de sus atributos se muestren en el
    #formulario antes de que el mismo sea modificado
    try:
        proyecto = Proyecto.objects.get(id=idP)
    except Proyecto.DoesNotExist:
        proyecto = None

    #Solamente se da acceso a modifcar el proyecto a los usuarios que tienen permisos necesarios o al admin
    usuario = Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto, 'modificar_proyecto') and not request.user.is_staff:
        return HttpResponseRedirect('/')
    #Cuando el usuario realice los cambios que quiera y de la opcion de guardar en el formulario
    #el request.method va a ser igual a 'POST'.
    if request.method=="POST":
        form = FormModificarProyecto(request.POST)
        #Se consulta si los datos enviados en el formulario son correctos
        if form.is_valid():
            #Django se encarga de validar y limpiar los datos segun el formulario que habiamos definido
            datos = form.cleaned_data
            #Se instancia el proyecto a ser modificado y se le asignan los cambios segun lo recibido en el formulario
            proyecto.nombre_corto = datos['nombre_corto_form']
            proyecto.nombre_largo = datos['nombre_largo_form']
            proyecto.descripcion = datos['descripcion_form']
            proyecto.fin = datos['fin_form']
            proyecto.save()
            #Se carga el mensaje que se va a mostrar en la pagina donde se redirige
            messages.success(request,"Se modifico exitosamente el proyecto")
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))
    else:
        form = FormModificarProyecto()

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'form':form,
        'idP':idP,
        'proyecto':proyecto,
        'permisosAsociados':permisosAsociados,

    }

    return render(request, "proyecto/modificar_proyecto.html", context)

def vista_eliminar_proyecto(request):
    """
    La funcion Vista "Eliminar Proyecto" borra de la base de datos el proyecto seleccionado
    @param request:
    @return: Se renderiza un html con el listado de proyectos del sistema despues de que el proyecto seleccionado fue eliminado
    """
    #Limita el acceso de esta vista solo al administrador del sistema
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    #Si se reciben datos del formulario se ingresa en el ciclo
    if(request.method=='POST'):
        idP = request.POST.get('idP')
        #Se busca en la base de datos el proyecto pasando como parametro su id correspondiente
        response = {}
        try:
            proyecto = Proyecto.objects.get(id=idP)
            #Se realiza una eliminacion en cascada sobre el proyecto, eliminando tambien sus relaciones con otras tablas
            proyecto.delete()

        except Proyecto.DoesNotExist:
            proyecto= None
        #Se devuelve un json vacio que es necesario para la funcion ajax dentro del template, esa funcion ajax
        #va a redireccionar al template de "Listar Proyectos"
        return JsonResponse(response)
    #Si una persona ingresa directamente "eliminar_proyecto" en la url se redirecciona al template "Listar Proyectos"
    return  HttpResponseRedirect(reverse('proyecto:vista_mis_proyectos'))

@login_required()
def vista_mis_proyectos(request):
    """
    La funcion Vista "Mis Proyectos" devuelve una lista con algunos detalles de todos los proyectos en donde el usuario
    logueado participa, si el usuario no participa en ningun proyecto se devuelve una lista vacia
    @param request:
    @return: Renderiza en un html la lista de proyectos en donde el usuario logueado participa
    """

    #Limita el acceso a los usuarios que no son administradores
    if request.user.is_staff:
        return HttpResponseRedirect('/')

    #Se intenta recuperar todos los proyectos de un usuario especificado, si no tiene proyectos se recupera un vacio
    usuario = get_object_or_404(Usuario, user=request.user)
    try:
        #Se crea una lista con todos los proyectos del usuario que solicita la funcion
        mis_proyectos = Usuario_Proyecto.objects.filter(usuario=usuario).all().distinct('proyecto_id')
    except Usuario_Proyecto.DoesNotExist:
        mis_proyectos = None

    context = {
        'mis_proyectos':mis_proyectos,
    }
    return render(request, "proyecto/mis_proyectos.html", context)

@login_required()
def vista_agregar_participantes(request,idP):

    """
    La funcion Vista "Agregar Participantes" permite seleccionar a las personas que van a trabajar en el proyecto,
    permite tambien seleccionar el rol que esas personas van a cumplir dentro del proyecto.
    @param request:
    @param idP: El identificador del Proyecto al cual se le va a agregar participantes
    @return:
    """
    #Se instancia el proyecto con el identificador que se recibe como parametro
    proyecto = get_object_or_404(Proyecto,id=idP)

    #Solamente se da acceso al rol que tiene los persmisos suficientes
    usuario = Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'agregar_participantes'):
        return HttpResponseRedirect('/')

    form = FormAgregarParticipantes()
    #Se listan los usuarios existentes en la base de datos, ordenados por su 'username'.
    lista_usuarios = User.objects.order_by('username').exclude(username='admin').all()
    lista_nombres_po = []
    #lista_usuarios es una lista de objetos User, por lo tanto solamente copiamos su campo 'username' en una lista de Strings
    for nombre_usuarios in lista_usuarios:
        if nombre_usuarios != request.user:
            lista_nombres_po.append(nombre_usuarios.username)
    lista_nombres_d = []
    for nombre_usuarios in lista_usuarios:
        lista_nombres_d.append(nombre_usuarios.username)

    #Se ingresa al ciclo una vez que el formulario es completado, en la primera llamada a la funcion no se ingresa
    if request.method == "POST":
        form = FormAgregarParticipantes(request.POST)
        #Se pregunta si el formulario es valido
        if form.is_valid():
            #Django se encarga de limpiar los datos del POST de acuerdo a lo especificado en nuestro formulario
            datos = form.cleaned_data
            #Se instancia los datos recabados desde el metodo POST
            user = get_object_or_404(User,username=datos['owner_form'])
            powner = get_object_or_404(Rol,nombre='Product Owner')

            #Para eliminar el anterior Product Owner si es que se cambio
            try:
                owner_actual = Usuario_Proyecto.objects.get(rol=powner, proyecto=proyecto)
                owner_actual.delete()
            except Usuario_Proyecto.DoesNotExist:
                owner_actual = None

            #Se asigna al nuevo Product Owner al proyecto
            try:
                usuario_owner = Usuario.objects.get(user=user)
                owner_proyecto= Usuario_Proyecto.objects.create(usuario=usuario_owner,proyecto=proyecto, rol=powner)
                owner_proyecto.save()
            except Usuario_Proyecto.DoesNotExist:
                owner_proyecto = None

            #Instanciamos el rol developer si existe
            developer = get_object_or_404(Rol,nombre='Developer')
            #Borrar developers antes de asignar los nuevos
            lista_developers_proyecto = Usuario_Proyecto.objects.filter(proyecto=proyecto,rol=developer).all()
            for x in lista_developers_proyecto:
                x.delete()
            #Obtener particpantes de checkboxes chequeados, procesar y guardar
            lista_participantes = request.POST.getlist('participadentes_form')
            #Se asignan todos los developers que fueron elegidos al proyecto
            for participante in lista_participantes:
                user = get_object_or_404(User,username= participante)
                usuario_developer = get_object_or_404(Usuario,user=user)
                try:
                    developer_proyecto = Usuario_Proyecto.objects.create(usuario=usuario_developer,proyecto=proyecto,rol=developer)
                    developer_proyecto.save()
                except Usuario_Proyecto.DoesNotExist:
                    developer_proyecto = None

            #Ya que por fin tenemos participantes en el proyecto, vamos a cambiar el estado del mismo a "Iniciado"
            if proyecto.estado =='EP':
                proyecto.estado='IN'
                proyecto.save()
                messages.success(request,'Se cambio el estado del proyecto a Iniciado')

            messages.success(request,'Se agregaron correctamente los participantes al proyecto')
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))
    else:
        #Instanciamos el rol developer si existe
        developer = get_object_or_404(Rol,nombre='Developer')
        #Seleccionamos solo los usuarios que son developers en el proyecto
        lista_developers_proyecto = Usuario_Proyecto.objects.filter(proyecto=proyecto,rol=developer).all()
        #Cargamos los nombres de los developers en una lista de strings
        lista_nombres_developers=[]
        for x in lista_developers_proyecto:
            lista_nombres_developers.append(x.usuario.user.username)
        #Creamos otra lista de strings que contiene solo la lista de usuarios que no son developers en el proyecto
        lista_usuarios_no_check = []
        for x in lista_nombres_d:
            if not x in lista_nombres_developers:
                lista_usuarios_no_check.append(x)

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context={
        'form':form,
        'idP':idP,
        'lista_nombres_po':lista_nombres_po,
        'lista_nombres_d':lista_nombres_d,
        'lista_nombres_developers':lista_nombres_developers,
        'lista_usuarios_no_check':lista_usuarios_no_check,
        'proyecto':proyecto,
        'permisosAsociados':permisosAsociados,
    }
    return render(request,"proyecto/agregar_participantes.html",context)

@login_required()
def vista_cancelar_proyecto(request,idP):
    """

    @param request: parametro recibido por defecto para las vistas de Django.
    @param idP: Identificador unico del proyecto seleccionado
    @return: Se devuelve un template en donde se da una razon por cancelar el proyecto
    """
    #Se instancia el proyecto con el identificador que se recibe como parametro
    proyecto = get_object_or_404(Proyecto,id=idP)

    #Solamente se da acceso al rol que tiene los persmisos suficientes
    usuario = Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'cancelar_proyecto'):
        return HttpResponseRedirect('/')

    #Si el proyecto ya esta cancelado no se muestra la pagina
    if proyecto.estado != 'CA':
        if request.method=='POST':
            proyecto.descripcion_cancelado = request.POST.get('idDescripcion_cancelado')
            proyecto.estado = 'CA'
            proyecto.fin = datetime.datetime.now()
            proyecto.save()
            messages.success(request,'El proyecto se Cancelo correctamente')
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))
    else:
        messages.success(request,'El proyecto actualmente se encuentra cancelado')
        return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'idP':idP,
        'proyecto':proyecto,
        'permisosAsociados':permisosAsociados,
    }
    return render(request,"proyecto/cancelar_proyecto.html",context)

@login_required()
def vista_retomar_proyecto(request,idP):
    """

    @param request: parametro recibido por defecto en las vistas de Django.
    @param idP: Identificador unico del proyecto seleccionado
    @return: Se renderiza al template de "Consulta de proyecto" con el estado del proyecto en "Iniciado".
    """
    #Se instancia el proyecto con el identificador que se recibe como parametro
    proyecto = get_object_or_404(Proyecto,id=idP)

    #Solamente se da acceso al rol que tiene los persmisos suficientes
    usuario = Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'modificar_proyecto'):
        return HttpResponseRedirect('/')

    #Se verifica que el proyecto este cancelado, si es el caso entonces se procede a cambiar el estado a iniciado
    if proyecto.estado == 'CA':
        proyecto.estado = 'IN'
        proyecto.fin = None
        proyecto.descripcion_cancelado = ""
        proyecto.save()
        messages.success(request,'El proyecto se retomo correctamente')

    return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))

@login_required()
def vista_finalizar_proyecto(request,idP):
    """

    @param request: parametro recibido por defecto para las vistas de Django.
    @param idP: Identificador unico del proyecto seleccionado
    @return: Se redirecciona al template de Detalles de un proyecto, con el estado de Finalizado
    """
    #Se instancia el proyecto con el identificador que se recibe como parametro
    proyecto = get_object_or_404(Proyecto,id=idP)

    #Solamente se da acceso al rol que tiene los persmisos suficientes
    usuario = Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'finalizar_proyecto'):
        return HttpResponseRedirect('/')

    #Solamente se termina el proyecto si esta iniciado previamente
    if proyecto.estado == 'IN':
        #Se crea una lista de los sprints del proyecto.
        sprint_terminados = True
        #Para que se pueda finalizar un proyecto todos los Sprints deben estar finalizados
        lista_sprint = Proyecto_Sprint_Us.objects.filter(proyecto=proyecto)
        for x in lista_sprint:
            if x.sprint.estado != 'FI':
                sprint_terminados = False

        if sprint_terminados == True:
            proyecto.estado = 'FI'
            proyecto.fin = datetime.datetime.now()
            proyecto.save()
            messages.success(request,"Se finalizo correctamente el proyecto")
        else:
            messages.success(request,"No se puede aun finalizar el proyecto")


    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'idP':idP,
        'proyecto':proyecto,
        'permisosAsociados':permisosAsociados,
    }

    return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))

@login_required()
def vista_modificar_SM(request,idP):
    """
    La funcion tiene como objetivo poder cambiar el SM de un proyecto
    @param request:
    @param idP: Identificador unico del proyecto seleccionado
    @return: Se muestra un template con las opciones para modificar el SM del proyecto
    """

    proyecto = get_object_or_404(Proyecto,id=idP)
    #Solamente se da acceso al rol que tiene los persmisos suficientes
    usuario = Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'modificar_SM'):
        return HttpResponseRedirect('/')

    #Se listan los usuarios existentes en la base de datos, ordenados por su 'username'.
    lista_usuarios = User.objects.order_by('username').exclude(username='admin').all()
    lista_nombres = []
    for x in lista_usuarios:
        if x.username != proyecto.get_Scrum().username:
            lista_nombres.append(x.username)

    #Añadir al SM nuevo
    if request.method == 'POST':
        try:
            #Se crea el usuario
            user_sm_nuevo = User.objects.get(username=request.POST.get('nuevo_SM'))
            usuario_sm_nuevo = Usuario.objects.get(user=user_sm_nuevo)
            #Se selecciona el rol de Scrum Master
            scrum = Rol.objects.get(nombre='Scrum Master')
            #Se hace el filtro para encontrar al proyecto
            proyecto_usuario = Usuario_Proyecto.objects.get(proyecto=proyecto, rol= scrum)
            #Se asigna el nuevo SM del proyecto
            proyecto_usuario.usuario = usuario_sm_nuevo
            proyecto_usuario.save()
            messages.success(request,"Se modifico exitosamente el SM del proyecto")
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))
        except Usuario_Proyecto.DoesNotExist, Usuario.DoesNotExist:
            usuario_sm_nuevo = None
            proyecto_usuario = None

        #Se carga el mensaje que se va a mostrar en la pagina donde se redirige
        messages.success(request,"No se modifico el SM del proyecto")
        return HttpResponseRedirect(reverse('proyecto:vista_consultar_proyecto', args=[idP]))

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)
    #if not (usuario.tienePermiso(proyecto, 'agregar_participantes') and
                #usuario.tienePermiso(proyecto, 'crear_us')):

    context= {
        'proyecto':proyecto,
        'permisosAsociados': permisosAsociados,
        'idP':idP,
        'lista_nombres':lista_nombres,
    }

    return render(request,"proyecto/modificar_sm.html",context)

