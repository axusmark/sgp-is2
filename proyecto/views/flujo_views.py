#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from proyecto.forms import *
from proyecto.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib import messages
from django.utils.datastructures import MultiValueDictKeyError
import datetime
from django.db.models import Max

@login_required
@csrf_protect
def vista_nuevoFlujo(request, idP):
    """
        La funcion vista "Nuevo Flujo_Proyecto" presenta un formulario para ingresar los datos de un nuevo flujo.
        Una vez creado el flujo exitosamente, redirecciona con alerta de confirmacion.
        @param request:
        @param idP: id del Proyecto.
        @return: Redirecciona a la lista de flujos, y presenta una alerta de confirmacion.
        """
    proyecto = Proyecto.objects.get(id=idP)
    usuario = Usuario.objects.get(user=request.user)
    # Chutar a usuario sin permisos
    if not usuario.tienePermiso(proyecto, 'crear_flujo'):
        return HttpResponseRedirect('/')
    if request.method != 'POST':
        idUs = Product_Backlog.objects.filter(proyecto=proyecto).values_list('us_id', flat=True)
        idUs = Flujo_Us.objects.filter(us_id__in=idUs).values_list('us_id', flat=True)
        idUs = Product_Backlog.objects.exclude(proyecto=proyecto, us_id__in=idUs).values_list('us_id', flat=True)
        lista_us = UserStory.objects.order_by('id').filter(id__in=idUs)


        # obtener permisos de un usuario
        permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
        return render(request, "html/flujos/nuevoFlujo.html", {'lista_us': lista_us,
                                                               'idP': idP,
                                                               'permisosAsociados': permisosAsociados,
                                                               'proyecto': proyecto,

                                                               })


    flujo = Flujo_Proyecto.objects.create(proyecto=proyecto,descripcion=request.POST['descripcionFlujo'])

    # procesar las actividades
    # Obtener nombre de checkboxes chequeados, procesar y guardar
    lista_checks_actividades = request.POST.getlist('chkActividades')
    cont = 1
    listaActividades = []
    for descripcionActividad in lista_checks_actividades:
        Actividad.objects.create(
            flujo=flujo,
            orden=cont,
            descripcion=descripcionActividad)
        cont = cont + 1

    # Registrar en log
    Log_Flujo.objects.create(flujo=flujo,
                             descripcion="Se ha creado el flujo.")

    messages.success(request, "Se creo exitosamente el flujo")

    return HttpResponseRedirect(reverse('proyecto:vista_listaFlujos', args=[idP]))


@login_required
def vista_listaFlujos(request, idP):
    """
        La funcion vista "Lista Flujos" presenta la lista de todos los flujos registrados.
        @param request:
        @param idP: id del Proyecto.
        @return: Renderiza el html encargado de presentar la lista de flujos del proyecto.
        """
    proyecto = Proyecto.objects.get(id=idP)
    usuario = Usuario.objects.get(user=request.user)
    # Chutar a usuario sin permisos
    if not usuario.tienePermiso(proyecto, 'ver_lista_flujos'):
        return HttpResponseRedirect('/')
    lista_flujos = Flujo_Proyecto.objects.order_by('id').filter(proyecto=proyecto)
    # obtener permisos de un usuario
    permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
    return render(request, "html/flujos/listaFlujos.html", {'lista_flujos': lista_flujos,
                                                            'proyecto': proyecto,
                                                            'idP': idP,
                                                            'permisosAsociados': permisosAsociados,
                                                            })


@login_required
def vista_verFlujo(request, idP, idFlujo):
    """
        La funcion vista "Ver Flujo_Proyecto" presenta los detalles de un flujo cuyo id se recibe por parámetro.
        También recibe el id del Proyecto asociado al mismo.
        @param request:
        @param idFlujo: ID del flujo
        @param idP: ID del proyecto
        @return: Renderiza el html encargado de presentar los detalles del flujo.
        """
    proyecto = Proyecto.objects.get(id=idP)
    usuario = Usuario.objects.get(user=request.user)
    # Chutar a usuario sin permisos
    if not usuario.tienePermiso(proyecto, 'ver_flujo'):
        return HttpResponseRedirect('/')
    # obtener permisos de un usuario

    permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
    flujo = get_object_or_404(Flujo_Proyecto, id=idFlujo)

    lista_actividades = Actividad.objects.order_by('orden').filter(flujo=flujo)
    idUs = Flujo_Us.objects.filter(flujo=flujo).values_list('us_id', flat=True)
    lista_actividad_us = Actividad_Us.objects.order_by('id').filter(us_id__in=idUs)
    listaActividadUS=[]
    #comprobar si el sprint en donde esta el us ya inicio
    for au in lista_actividad_us:
        try:
            sprint=Proyecto_Sprint_Us.objects.get(proyecto=proyecto,us=au.us).sprint
            listaActividadUS.append([au,sprint.estaEnEjecucion()])
        except Proyecto_Sprint_Us.DoesNotExist:
            sprint = None


    # enviar lista de us en los que el usuario es developer
    usuario = Usuario.objects.get(user=request.user)

    idListaUS = Product_Backlog.objects.filter(proyecto=proyecto, usuario=usuario).values_list('us_id', flat=True)
    lista_us_esDeveloper = UserStory.objects.filter(id__in=idListaUS)

    # obtener lista de US actuales del flujo para enviar a los checks del form
    usActuales = UserStory.objects.filter(id__in=idUs)

    #obtener log del flujo
    lista_log=Log_Flujo.objects.order_by('id').filter(flujo=flujo)
    #obtener ultima actividad del flujo
    try:
        ultimaActividad= Actividad.objects.filter(flujo=flujo).latest('orden')
    except Actividad.DoesNotExist:
        ultimaActividad=False

    return render(request, "html/flujos/verFlujo.html", {'flujo': flujo,
                                                         'lista_actividades': lista_actividades,
                                                         'lista_actividad_us': listaActividadUS,
                                                         'idP': idP,
                                                         'ultimaActividad': ultimaActividad,
                                                         'permisosAsociados': permisosAsociados,
                                                         'proyecto': proyecto,
                                                         'lista_us_esDeveloper': lista_us_esDeveloper,
                                                         'usActuales': usActuales,
                                                         'lista_log':lista_log,

                                                         })


@login_required
@csrf_protect
def vista_editarFlujo(request, idP, idFlujo):
    """
        La funcion vista "Editar Flujo_Proyecto" presenta un formulario para modificar los datos actuales de un flujo
        cuyo ID se recibe por parámetro.
        @param request:
        @param idFlujo: ID del proyecto
        @param idFlujo: ID del flujo
        @return: Redirecciona a la vista "Lista de Flujos", con alerta de confirmacion de cambios.
        """
    proyecto = Proyecto.objects.get(id=idP)
    usuario = Usuario.objects.get(user=request.user)
    # Chutar a usuario sin permisos, tambien chutar si es que ya hay us en el flujo

    if not usuario.tienePermiso(proyecto, 'editar_flujo'):
        return HttpResponseRedirect('/')
    if request.method == 'POST':

        flujo = Flujo_Proyecto.objects.get(id=idFlujo)
        flujo.descripcion = request.POST['descripcionFlujo']

        flujo.save()
        # Obtener nombre de checkboxes chequeados en Actividades
        try:

            lista_checks_actividades = request.POST.getlist('chkActividades')

            # reorganizar actividades que ya estan guardadas

            # pueden haber dos tipos de valores recibidos> un ID si actividad ya existe
            # o un string de descripcion si es que aun no existe dicha actividad

            # verificar los Ids y setear el orden de los mismos
            cont = 1
            for actividad in lista_checks_actividades:
                try:
                    a = Actividad.objects.get(id=actividad)
                    a.orden = cont
                    a.save()

                except ValueError:
                    # se han ingresado nuevas actividades, por tanto se recibio un string
                    Actividad.objects.create(flujo=flujo, orden=cont, descripcion=actividad)
                cont = cont + 1
        except MultiValueDictKeyError:
            print ""

        # procesar los us
        lista_checks_us = request.POST.getlist('chkUs')
        for iDus in lista_checks_us:
            us = UserStory.objects.get(id=iDus)
            flujoUs = Flujo_Us(flujo=flujo, us=us)
            flujoUs.save()

            # relacionar actividad con estado inicial
            actividad = Actividad.objects.get(flujo=flujo, orden=1)
            au = Actividad_Us(actividad=actividad, us=us, estado='TD')
            au.save()
        # Registrar en log
        Log_Flujo.objects.create(flujo=flujo,
                                     descripcion="Se ha modificado los datos del flujo.")
        messages.success(request, "Se modificó exitosamente el flujo")
        return HttpResponseRedirect(reverse('proyecto:vista_listaFlujos', args=[idP]))

    flujo = get_object_or_404(Flujo_Proyecto, id=idFlujo)

    # obtener lista de US actuales del flujo
    # no permitir ejecucion de esta vista si existe us actuales
    idUs = Flujo_Us.objects.filter(flujo=flujo).values_list('us_id', flat=True)
    usActuales = UserStory.objects.filter(id__in=idUs)


    if len(usActuales) > 0:
        return HttpResponseRedirect('/')


    # obtener permisos de un usuario

    permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)


    lista_actividades = Actividad.objects.order_by('orden').filter(flujo=flujo)
    idUs = Flujo_Us.objects.filter(flujo=flujo).values_list('us_id', flat=True)
    lista_us = UserStory.objects.filter(id__in=idUs)




    return render(request, "html/flujos/editarFlujo.html", {'flujo': flujo,
                                                            'idP': idP,
                                                            'lista_actividades': lista_actividades,
                                                            'lista_us': lista_us,
                                                            'permisosAsociados': permisosAsociados,
                                                            'proyecto': proyecto,
                                                            'usActuales': usActuales,
                                                            })


@login_required()
@csrf_protect
@login_required()
def vista_editarEstadoUS(request,idP,idFlujo,idUs):
    """
        La funcion vista "Editar Estado US"  actualiza el estado de un User Story, correspondiente a algun flujo.
        La ID del User Story se recibe por POST
        @param request:
        @return: Redirecciona a la vista "Ver Flujo_Proyecto".
        """
    proyecto = Proyecto.objects.get(id=idP)
    us = UserStory.objects.get(id=idUs)
    #Chutar a usuario sin permiso
    try:
        sprint=Proyecto_Sprint_Us.objects.get(proyecto=proyecto,us=us).sprint
        estaEjecucion=sprint.estaEnEjecucion()
        esDeveloper=us.esDeveloper(user=request.user,proyecto=proyecto)
        if proyecto.get_Scrum() == request.user:
            esScrum=True
        else:
            esScrum=False

        if esDeveloper or esScrum:
            usuarioTienePermiso=True
        else:
            usuarioTienePermiso=False

        if not estaEjecucion or not usuarioTienePermiso:
            return HttpResponseRedirect("/")
    except ObjectDoesNotExist:
            return HttpResponseRedirect("/")

    actividadUsActual = Actividad_Us.objects.get(us=us)
    actividadActual=actividadUsActual.actividad
    flujo = Flujo_Proyecto.objects.get(id=idFlujo)
    if request.method == 'POST':
        # atrapar caso de uso: envio de POST desde modo developer

        try:

            actividadRecibida = Actividad.objects.get(id=request.POST['actividadActual'], flujo=flujo)
            estado = request.POST['estado']
            # para mover a Doing de actividad anterior (si existe actividad anterior)

            if estado == 'Doing (Actividad anterior)':
                anteriorActividad = Actividad.objects.get(flujo=flujo, orden=actividadActual.orden - 1)
                estado = 'DO'
                actividadUsActual.actividad = anteriorActividad
                actividadUsActual.estado = estado
                actividadUsActual.save()

                # Registrar en el log del US
                Log_Us.objects.create(us=us, descripcion="Se modifico avance de estado en el flujo: "+flujo.descripcion)
                Log_Flujo.objects.create(flujo=flujo, descripcion="Se modifico avance de estado del User Story '"+us.descripcion_corta+"'.")
                messages.warning(request, "El User Story ha pasado a la actividad anterior (Doing)")
                return HttpResponseRedirect(reverse('proyecto:vista_verFlujo', args=[idP, idFlujo]))
            if estado == 'To Do (Actividad siguiente)':
                siguienteActividad = Actividad.objects.get(flujo=flujo, orden=actividadActual.orden + 1)
                estado = 'TD'
                actividadUsActual.actividad = siguienteActividad
                actividadUsActual.estado = estado
                actividadUsActual.save()

                # Registrar en el log del US
                Log_Us.objects.create(us=us,descripcion="Se avanzo a la actividad siguiente (To Do) en el flujo: " + flujo.descripcion)
                Log_Flujo.objects.create(flujo=flujo,descripcion="El User Story '" + us.descripcion_corta + "' avanzo a la actividad siguiente (To Do).")
                messages.warning(request, "El User Story ha pasado a la actividad siguiente (To Do)")
                return HttpResponseRedirect(reverse('proyecto:vista_verFlujo', args=[idP, idFlujo]))
            if estado == 'To Do':
                estado = 'TD'
            elif estado == 'Doing':
                estado = 'DO'
            else:
                estado = 'DN'
            # Se procede a cambiar el estado
            actividadUsActual.estado = estado
            actividadUsActual.save()
            #detectar si finaliza ultima actividad con este cambio de estado
            try:
                siguienteActividad = Actividad.objects.get(flujo=flujo, orden=actividadActual.orden + 1)
                # Registrar en el log del US
                Log_Us.objects.create(us=us,descripcion="Se avanzo al siguiente estado en el flujo: " + flujo.descripcion)
                Log_Flujo.objects.create(flujo=flujo,descripcion="El User Story '" + us.descripcion_corta + "' avanzo al siguiente estado.")

                messages.success(request, "Se modifico exitosamente el estado del User Story")
            except ObjectDoesNotExist:
                if estado == 'DN':
                    # Registrar en el log del US
                    Log_Us.objects.create(us=us, descripcion="Se finalizo las actividades del flujo: " + flujo.descripcion)

                    #Enviar mensaje al SM
                    asunto = "User Story finalizado"

                    mensaje = "SGP\nFecha de envío: " + str(datetime.datetime.now()) + "\n\nEL User Story con descripción corta: "+ str(us.descripcion_corta) + ".\nFinalizo correctamente en el Flujo: " + str(flujo.descripcion) + "\n\n\nObservación: Mensaje generado y envíado automáticamente"

                    SM = proyecto.get_Scrum()
                    destinatario = []
                    destinatario.append(SM.email)
                    try:
                        send_mail(asunto, mensaje, settings.EMAIL_HOST_USER ,destinatario,fail_silently=False)
                        messages.warning(request, "El User Story ha finalizado el flujo. Se notifico al Scrum Master vía Mail")
                    except:
                        messages.warning(request, "El User Story ha finalizado el flujo. No se pudo realizar la notificación al Scrum Master vía Mail")
                else:
                    # Registrar en el log del US
                    Log_Us.objects.create(us=us,descripcion="Se modifico estado en el flujo: " + flujo.descripcion)
                    Log_Flujo.objects.create(flujo=flujo,descripcion="Se ha modificado el estado del User Story '" + us.descripcion_corta + "'.")
                    messages.success(request, "Se modifico exitosamente el estado del User Story")
            return HttpResponseRedirect(reverse('proyecto:vista_verFlujo', args=[idP, idFlujo]))
        except MultiValueDictKeyError:
            #Obtener la actividad que corresponde al recibido por form

            actividadChk=Actividad.objects.get(descripcion=request.POST['actividad'],flujo=flujo)
            estado = request.POST['estado']



            # si la actividad recibida por form es distinta a la actual, se procede a actualizar la actividad en la bd
            if actividadChk != actividadActual:
                actividadUsActual.actividad = actividadChk
                actividadUsActual.save()
                # actualizar puntero de actividad actual
                actividadActual = actividadUsActual.actividad

            estado=request.POST['estado']

            if estado == 'To Do':
                estado ='TD'
            elif estado == 'Doing':
                estado= 'DO'
            else:
                #es el ultimo estado en la actividad, se procede a pasar como TD en la siguiente actividad
                #verificar si esta es la ultima actividad
                try:
                    siguienteActividad = Actividad.objects.get(flujo=flujo, orden=actividadActual.orden + 1)
                    estado = 'TD'
                    actividadUsActual.actividad = siguienteActividad
                    actividadUsActual.estado = estado
                    actividadUsActual.save()
                    # Registrar en el log del US
                    Log_Us.objects.create(us=us,descripcion="Se avanzo a la actividad siguiente (To Do) en el flujo: " + flujo.descripcion)
                    Log_Flujo.objects.create(flujo=flujo,descripcion="El User Story '" + us.descripcion_corta + "' avanzo a la actividad siguiente (To Do).")
                    messages.warning(request, "El User Story ha pasado a la siguiente actividad")
                except ObjectDoesNotExist:
                    estado = 'DN'
                    actividadUsActual.estado = estado
                    actividadUsActual.save()
                    # Registrar en el log del US

                    Log_Us.objects.create(us=us,descripcion="Se finalizo las actividades del flujo: " + flujo.descripcion)

                    #Enviar mensaje al SM
                    asunto = "User Story finalizado"

                    mensaje = "SGP\nFecha de envío: " + str(datetime.datetime.now()) + "\n\nEL User Story con descripción corta: "+ str(us.descripcion_corta) + ".\nFinalizo correctamente en el Flujo: " + str(flujo.descripcion) + "\n\n\nObservación: Mensaje generado y envíado automáticamente"
                    #mensaje = "<p><u><strong>EL User Story con descripción corta:</strong></u>" + str(us.descripcion_corta)+"</p><h3>FINALIZO CORRECTAMENTE</h3><p><u><strong>En el Flujo:</strong></u>" + str(flujo.descripcion)+"</p><p><u><strong>Fecha de envío:</strong></u>" +str(datetime.datetime.now())+"</p>"

                    SM = proyecto.get_Scrum()
                    destinatario = []
                    destinatario.append(SM.email)
                    try:
                        send_mail(asunto, mensaje, settings.EMAIL_HOST_USER ,destinatario,fail_silently=False)
                        messages.warning(request, "El User Story ha finalizado la última actividad del flujo actual."+
                                              "Se notifico al Scrum Master vía Mail")
                    except:
                        messages.warning(request, "El User Story ha finalizado la última actividad del flujo actual."+
                                              "No se pudo realizar la notifiación vía Mail")

                    return HttpResponseRedirect(reverse('proyecto:vista_verFlujo', args=[idP, idFlujo]))
            #La actividad recibida por form no es Done
            #Se procede a cambiar normalmente el estado
            actividadUsActual.estado = estado
            actividadUsActual.save()
            # Registrar en el log del US
            Log_Us.objects.create(us=us, descripcion="Se modifico estado en el flujo: " + flujo.descripcion)
            Log_Flujo.objects.create(flujo=flujo,
                                    descripcion="Se ha modificado el estado del User Story '" + us.descripcion_corta + "'.")
            messages.success(request, "Se modifico exitosamente el estado del User Story")
            return HttpResponseRedirect(reverse('proyecto:vista_verFlujo', args=[idP, idFlujo]))

    permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
    au=Actividad_Us.objects.get(us=us)
    estado = au.get_estado_verbose()

    #Enviar actividad actual del us

    listaActividades=Actividad.objects.order_by("orden").filter(flujo=flujo)

    # verificar si el user es developer del US, debe tener otro tipo de formulario
    esDeveloper = us.esDeveloper(request.user, proyecto)
    hayActividadAnterior=False
    hayActividadSiguiente = False
    if esDeveloper:
        #verificar si existe actividad anterior
        try:
            Actividad.objects.get(flujo=flujo, orden=actividadActual.orden - 1)
            hayActividadAnterior=True
        except ObjectDoesNotExist:
            hayActividadAnterior = False
        #verificar si existe actividad siguiente
        try:
            Actividad.objects.get(flujo=flujo, orden=actividadActual.orden + 1)
            hayActividadSiguiente = True
        except ObjectDoesNotExist:
            hayActividadSiguiente = False
    return render(request, "html/flujos/editarEstadoUS.html", {'idP': idP,
                                                               'idFlujo': idFlujo,
                                                            'permisosAsociados': permisosAsociados,
                                                            'proyecto': proyecto,
                                                            'estado':estado,
                                                            'listaActividades': listaActividades,
                                                            'esDeveloper': esDeveloper,
                                                            'actividadActual': actividadActual,
                                                            'hayActividadAnterior': hayActividadAnterior,
                                                            'hayActividadSiguiente': hayActividadSiguiente
                                                               })


@login_required
@csrf_protect
def vista_editarActividad(request, idP, idFlujo, idActividad):
    """
        La funcion vista "Editar Actividad" presenta un formulario para modificar el nombre de una actividad.
        Una vez modificada la actividad exitosamente, redirecciona a la vista 'Ver Flujo_Proyecto' con alerta de confirmacion.
        @param request:
        @param idP: ID del proyecto
        @param idFlujo: ID del flujo
        @param idActividad: ID de la actividad
        @return: redirecciona a la vista "Editar Flujo_Proyecto", con una alerta de confirmacion.
        """
    proyecto = Proyecto.objects.get(id=idP)
    usuario = Usuario.objects.get(user=request.user)
    # Chutar a usuario sin permisos
    if not usuario.tienePermiso(proyecto, 'editar_actividad'):
        return HttpResponseRedirect('/')
    actividad = Actividad.objects.get(id=idActividad)
    if request.method != 'POST':
        # obtener permisos de un usuario

        permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
        return render(request, "html/flujos/editarActividad.html", {'idP': idP,
                                                                    'idFlujo': idFlujo,
                                                                    'permisosAsociados': permisosAsociados,
                                                                    'proyecto': proyecto,
                                                                    'actividad': actividad
                                                                    })
    else:
        nombreViejo=actividad.descripcion
        actividad.descripcion = request.POST['nombre']
        actividad.save()
        Log_Flujo.objects.create(flujo_id=idFlujo,descripcion="Se ha modificado el nombre de una actividad: de '"+nombreViejo+"' a '"+actividad.descripcion+"' ")
        messages.success(request, "Se actualizo correctamente la actividad")
        return HttpResponseRedirect(reverse('proyecto:vista_editarFlujo', args=[idP, idFlujo]))

@login_required
def vista_agregarUSaFlujo(request, idP,idUS,idS):
    """
    La funcion vista "Agregar US a flujo" permite añadir US a un flujo.
    cuyo ID se recibe por parámetro.
    @param request:
    @param idP: ID del proyecto.
    @param idUS: ID del User Story
    @return: Renderiza el html encargado de presentar los detalles del sprint, con una alerta de confirmacion de
    persistencia.
    """
    #chutar de la vista si el us ya esta en un flujo

    try:
        Flujo_Us.objects.get(us_id=idUS)
        return HttpResponseRedirect(reverse('proyecto:vista_consultar_sprint', args=[idP, idS]))
    except ObjectDoesNotExist:
        if request.method == 'POST':
            us=UserStory.objects.get(id=idUS)
            idFlujo=request.POST['idFlujo']
            flujo=Flujo_Proyecto.objects.get(id=idFlujo)
            flujoUs = Flujo_Us(flujo_id=idFlujo, us=us)
            flujoUs.save()

            # relacionar actividad con estado inicial
            actividad = Actividad.objects.get(flujo_id=idFlujo, orden=1)
            au = Actividad_Us(actividad=actividad, us=us, estado='TD')
            au.save()

            # Registrar esta creacion en el log
            Log_Us.objects.create(us=us, descripcion="Se ha agregado al flujo: "+flujo.descripcion)
            Log_Flujo.objects.create(flujo=flujo,descripcion="El User Story '"+us.descripcion_corta+"' se ha agregado al flujo.")
            messages.success(request, "Se agrego correctamente el User Story al flujo.")
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_sprint', args=[idP, idS]))

    #obtener lista de flujos (DEBEN TENER UN ESTADO QUE CONTROLE SI SE PUEDE SEGUIR AÑADIENDO US)
    proyecto = Proyecto.objects.get(id=idP)
    listaFlujos= Flujo_Proyecto.objects.filter(proyecto=proyecto)


    permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
    return render(request, "html/flujos/anhadirUSaFlujo.html", {'idP': idP,
                                                               'permisosAsociados': permisosAsociados,
                                                               'proyecto': proyecto,
                                                               'listaFlujos': listaFlujos,
                                                               'idS':idS,
                                                               })




