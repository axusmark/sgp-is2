#!/usr/bin/env python
# -*- coding: utf-8 -*-
import base64

from django.contrib import messages
from django.shortcuts import render
from proyecto.models import *
from proyecto.forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponse
from proyecto.forms import UploadFileForm

# Create your views here.
@login_required()
@csrf_protect
def vista_crear_userstory(request,idP):
    """
    Metodo para la creacion de un UserStory, al crear se solicita las descripciones,
    prioridad y la duracion estimada de desarrollar dicho US
    @param request:
    @return: Renderiza un html que contiene un formulario para crear un UserStory.
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles crear user stories
    proyecto = Proyecto.objects.get(id=idP)
    usuario= Usuario.objects.get(user=request.user)
    if not usuario.tienePermiso(proyecto,'crear_us'):
        return HttpResponseRedirect('/')
    msj_error=''
    # Una vez completado el formulario se pregunta si el metodo de paso fue post y de haber sido True
    if request.method=="POST":
        form = FormCrearUserStory(request.POST)
        # Se pregunta si los datos ingresados son validos
        print form.is_valid()
        if form.is_valid():
            #Django se encarga de adecuar y limpiar los datos de acuerdo a lo especificado en el form
            datos= form.cleaned_data
            userstory = UserStory()
            userstory.descripcion_corta = datos['descripcion_corta_form']
            userstory.descripcion_larga = datos['descripcion_larga_form']
            userstory.tiempo_estimado = datos['tiempo_estimado_form']
            prioridad = datos['prioridad_form']
            if prioridad == 'Alta':
                prioridad = 'AL'
            elif prioridad=='Media':
                prioridad = 'ME'
            elif prioridad == 'Baja':
                prioridad = 'BA'
            userstory.prioridad = prioridad
            userstory.valor_de_negocios = datos['valor_de_negocios_form']
            userstory.valor_tecnico = datos['valor_tecnico_form']
            #Calculos para ordenar los US's por prioridad, teniendo en cuenta sus valores tecnicos y de negocio
            if userstory.prioridad == 'AL':
                userstory.valor_prioridad = float((userstory.valor_de_negocios + 80 + (2*userstory.valor_tecnico)))/4
            elif userstory.prioridad == 'ME':
                userstory.valor_prioridad = float((userstory.valor_de_negocios + 40 + (2*userstory.valor_tecnico)))/4
            elif userstory.prioridad == 'BA':
                userstory.valor_prioridad = float((userstory.valor_de_negocios + 1 + (2*userstory.valor_tecnico)))/4
            userstory.save()

            #relacionar a product backlog del proyecto cuya ID es recibida por parametro URL
            product_backlog= Product_Backlog(us=userstory,proyecto=proyecto)
            product_backlog.save()

            #Registrar esta creacion en el log del US
            Log_Us.objects.create(us=userstory, descripcion="Se ha creado el User Story en el Product "+
                                                        "Backlog del Proyecto: '"+proyecto.nombre_corto+"'")
            # Mensaje de que se ha creado un US con exito
            messages.success(request,"Se creo exitosamente el US")
            return HttpResponseRedirect(reverse('proyecto:vista_lista_us', args=[idP]))
        else:
            msj_error = 'Ten en cuenta las indicaciones sobre los campos'
    else:
        # Si no se cargaron los datos se devuelve un formulario vacio
        form = FormCrearUserStory

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'form': form,
        'msj_error': msj_error,
        'proyecto':proyecto,
        'idP':idP,
        'permisosAsociados':permisosAsociados,
    }

    return render(request, 'html/us/crear_userstory.html', context)

@login_required()
def vista_eliminarUS(request):
    """
        La funcion vista "Eliminar US" elimina el User Story cuyo ID del mismo y del proyecto
        al que pertenece, se reciben por POST
        @param request:
        @return: Redirecciona a la vista "Consulta proyecto".
        """
    if request.method== "POST":
        IDProyecto = request.POST['IDProyecto']
        IDUs= request.POST['IDUs']
        # Chutar a usuario indeseado
        usuario = Usuario.objects.get(user=request.user)
        proyecto=Proyecto.objects.get(id=IDProyecto)

        if not usuario.tienePermiso(proyecto, 'eliminar_us'):
            return HttpResponseRedirect('/')

        us = UserStory.objects.get(id=IDUs)
        us.delete()
        return HttpResponseRedirect('/'+IDProyecto+'/consultar_proyecto')
    return HttpResponseRedirect('/')

@login_required()
def vista_consultar_us(request,idP,idUs):
    """
    La funcion vista "Consultar US" devuelve los detalles de un US seleccionado asociado al proyecto
    @param request: se recibe por defecto en las vistas de Django.
    @param idP: El identificador del Proyecto al cual pertenece el US
    @param idUs: El identificador unico del User Story
    @return: Renderiza un template que contiene los detalles del US seleccionado
    """
    #Solamente se da acceso a los detalles a las personas que participan de alguna manera en el proyecto o
    #al administrador del sistema
    proyecto = get_object_or_404(Proyecto,id=idP)
    usuario_participante = get_object_or_404(Usuario,user=request.user)
    us = get_object_or_404(UserStory,id=idUs)
    try:
        usuario_en_proyecto = Usuario_Proyecto.objects.filter(usuario=usuario_participante, proyecto=proyecto).all()
    except Usuario_Proyecto.DoesNotExist:
        usuario_en_proyecto = None
    #Se verifica que el usuario que quiera ver el US este asociado al proyecto o sea el administrador
    if not usuario_en_proyecto and not request.user.is_staff:
        return HttpResponseRedirect('/')

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    #obtener el Developer del User Story
    pb = get_object_or_404(Product_Backlog,proyecto=proyecto,us=us)

    #obtener archivos adjuntos del User Story
    files=Archivo_Us.objects.filter(us=us)

    #obtener notas adjuntas del User Story
    notas= Nota_Us.objects.filter(us=us)

    #obtener log del us
    lista_log=Log_Us.objects.order_by('id').filter(us=us)

    #esDeveloper
    esDeveloper=us.esDeveloper(request.user,proyecto)
    if proyecto.get_Scrum() == request.user:
        esScrumMaster =True
    else:
        esScrumMaster=False
    context = {
        'proyecto':proyecto,
        'idP':idP,
        'idUs':idUs,
        'us':us,
        'pb':pb,
        'files':files,
        'notas': notas,
        'lista_log':lista_log,
        'permisosAsociados':permisosAsociados,
        'esDeveloper':esDeveloper,
        'esScrumMaster':esScrumMaster,
    }
    return render(request, 'html/us/consultar_us.html',context)

@login_required()
def vista_lista_us(request, idP):
    """
    La funcion vista "Listar US" devuelve la lista de todos los User Stories asociados a un determinado proyecto
    @param request: parametro por defecto de las vistas de Django
    @param idP: Identificador unico del proyecto al cual estan asociados los User Stories
    @return: Se retorna un template renderizado con la lista de proyectos
    """
    #Solamente se da acceso a los detalles a las personas que participan de alguna manera en el proyecto o
    #al administrador del sistema
    proyecto = get_object_or_404(Proyecto,id=idP)
    usuario_participante = get_object_or_404(Usuario,user=request.user)
    try:
        usuario_en_proyecto = Usuario_Proyecto.objects.filter(usuario=usuario_participante, proyecto=proyecto).all()
    except Usuario_Proyecto.DoesNotExist:
        usuario_en_proyecto = None
    #Se verifica que el usuario que quiera ver el US este asociado al proyecto o sea el administrador
    if not usuario_en_proyecto and not request.user.is_staff:
        return HttpResponseRedirect('/')

    #Se crea una lista de proyectos si es que existen US
    ids_pb= Product_Backlog.objects.filter(proyecto=proyecto).values_list('us_id',flat=True)
    lista_us= UserStory.objects.filter(id__in =ids_pb).order_by('-valor_prioridad')

    # Establecer permisos para renderizar el boton Adjuntar archivos y notas
    # Enviar lista de developers al template, para comparar con el user visitante
    # Lista de developers asociados al proyecto


    listaUs = []
    for i in lista_us:
        listaUs.append([i, i.get_developer(proyecto)])

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)
    #if not (usuario.tienePermiso(proyecto, 'agregar_participantes') and
    #usuario.tienePermiso(proyecto, 'crear_us')):
    scrum=proyecto.get_Scrum()
    context = {
        'proyecto':proyecto,
        'lista_us':listaUs,
        'permisosAsociados': permisosAsociados,
        'idP':idP,
        'scrum':scrum,
    }

    return render(request,"html/us/lista_us.html",context)

@login_required()
@csrf_protect
def vista_modificar_us(request,idP,idUs):
    """
    La funcion vista "Modificar User Story" permite al Scrum Master del proyecto modificar una User Story
    seleccionada
    @param request: parametro por defecto de las vistas de Django
    @param idP: Identificador unico del proyecto
    @param idUs: Identificador unico del User Story asociado al proyecto
    @return: Se renderiza un template con los detalles que se pueden modificar del User Story
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles modificar user stories
    proyecto = get_object_or_404(Proyecto,id=idP)
    usuario= get_object_or_404(Usuario,user=request.user)
    if not usuario.tienePermiso(proyecto,'editar_us'):
        return HttpResponseRedirect('/')

    #Cuando el usuario realice los cambios que quiera y de la opcion de guardar en el formulario
    #el request.method va a ser igual a 'POST'.
    us = get_object_or_404(UserStory,id=idUs)
    if request.method=="POST":
        form = FormModificarUserStory(request.POST)
        #Se consulta si los datos enviados en el formulario son correctos
        if form.is_valid():
            #Django se encarga de validar y limpiar los datos segun el formulario que habiamos definido
            datos = form.cleaned_data
            #Se instancia el US a ser modificado y se le asignan los cambios segun lo recibido en el formulario
            us.descripcion_corta = datos['descripcion_corta_form']
            us.descripcion_larga = datos['descripcion_larga_form']
            us.tiempo_estimado = datos['tiempo_estimado_form']
            prioridad = datos['prioridad_form']
            if prioridad == 'Alta':
                prioridad = 'AL'
            elif prioridad=='Media':
                prioridad = 'ME'
            elif prioridad == 'Baja':
                prioridad = 'BA'
            us.prioridad = prioridad
            us.valor_de_negocios = datos['valor_de_negocios_form']
            us.valor_tecnico = datos['valor_tecnico_form']
            #Calculos para ordenar los US's por prioridad, teniendo en cuenta sus valores tecnicos y de negocio
            if us.prioridad == 'AL':
                us.valor_prioridad = float((us.valor_de_negocios + 80 + (2*us.valor_tecnico)))/4
            elif us.prioridad == 'ME':
                us.valor_prioridad = float((us.valor_de_negocios + 40 + (2*us.valor_tecnico)))/4
            elif us.prioridad == 'BA':
                us.valor_prioridad = float((us.valor_de_negocios + 1 + (2*us.valor_tecnico)))/4
            us.save()
            # Registrar este evento en el log del US
            Log_Us.objects.create(us=us, descripcion="Se ha modificado los datos del User Story.")
            #Se carga el mensaje que se va a mostrar en la pagina donde se redirige
            messages.success(request,"Se modifico exitosamente el User Story")
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_us', args=[idP,idUs]))
    else:
        form = FormModificarUserStory()

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        "form":form,
        'idP':idP,
        'proyecto':proyecto,
        'us':us,
        'idUs':idUs,
        'permisosAsociados':permisosAsociados,
    }

    return render(request, "html/us/modificar_us.html", context)

@login_required()
@csrf_protect
def vista_registrar_avance_us(request,idP,idUs):
    """
    La vista permite al usuario con permisos suficientes realizar un registro de las horas dedicadas al US.
    @param request: parametro por defecto de las vistas de Django
    @param idP: Identificador unico del proyecto
    @param idUs: Identificador unico del User Story asociado al proyecto
    @return: Se renderiza un template para que el usuario pueda registrar el avance en horas del US.
    """
    #Solo se permite el acceso a las personas que tienen asociados a sus roles modificar user stories
    proyecto = get_object_or_404(Proyecto,id=idP)
    usuario= get_object_or_404(Usuario,user=request.user)
    if not usuario.tienePermiso(proyecto,'registrar_avances_us'):
        return HttpResponseRedirect('/')

    #Se instancia el US en la cual se quieren registrar los cambios
    us = get_object_or_404(UserStory,id=idUs)
    if request.method == 'POST':
        form = FormRegistarAvanceUs(request.POST)
        if form.is_valid():
            #Django se encarga de validar y limpiar los datos segun el formulario que habiamos definido
            datos = form.cleaned_data
            us.tiempo_dedicado = us.tiempo_dedicado + datos['tiempo_dedicado_form']
            us.save()
            messages.success(request,"Se actualizo correctamente el tiempo dedicado al US")
            return HttpResponseRedirect(reverse('proyecto:vista_consultar_us', args=[idP,idUs]))

    #obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos= usuario.getListaPermisos(proyecto).values_list('permiso_id',flat=True)
    permisosAsociados=Permiso.objects.filter(id__in = idPermisos).values_list('nombre',flat=True)

    context = {
        'idP':idP,
        'idUs':idUs,
        'proyecto':proyecto,
        'us':us,
        'permisosAsociados':permisosAsociados,
    }

    return render(request,"html/us/avance_us.html",context)

@login_required()
def vista_subirArchivo(request,idP,idUs):
    """
        La funcion vista "Subir Archivo" recibe el archivo y/o nota adjunta por POST
        @param request:
        @param idP: ID del proyecto.
        @param idUS: ID del User Story
        @return: Redirecciona a la vista "Consultar User Story".
        """
    # Solo se permite el acceso a los roles que pueden subir archivos
    proyecto = get_object_or_404(Proyecto, id=idP)
    usuario = get_object_or_404(Usuario, user=request.user)
    if not usuario.tienePermiso(proyecto, 'subir_archivo'):
        return HttpResponseRedirect('/')
    us = UserStory.objects.get(id=idUs)

    #Permitir acceso solo a developer del us o Scrum master del proyecto
    if proyecto.get_Scrum().id != request.user.id:
        developer = us.get_developer(proyecto)
        if developer is None or us.get_developer(proyecto).id != request.user.id:
            return HttpResponseRedirect('/')

    if request.method == 'POST':

        files = request.FILES.getlist('archivo')
        for file in files:
            fileB64= str(base64.b64encode(file.read()))

            au=Archivo_Us(us=us,archivo=fileB64,nombreArchivo=file.name,user=request.user)
            au.save()
        if request.POST['nota'] !='':
            nu=Nota_Us(us=us,nota=request.POST['nota'],user=request.user)
            nu.save()
        messages.success(request, "User Story actualizado correctamente.")
        return HttpResponseRedirect(reverse('proyecto:vista_consultar_us', args=[idP,idUs]))
    proyecto=Proyecto.objects.get(id=idP)

    # obtener permisos de un usuario
    usuario = Usuario.objects.get(user=request.user)
    idPermisos = usuario.getListaPermisos(proyecto).values_list('permiso_id', flat=True)
    permisosAsociados = Permiso.objects.filter(id__in=idPermisos).values_list('nombre', flat=True)
    context = {
        'idP': idP,
        'idUs': idUs,
        'proyecto': proyecto,
        'us': us,
        'permisosAsociados': permisosAsociados,
    }
    return render(request, 'html/us/subirArchivoYnotas.html',context)

@login_required()
def vista_eliminarArchivo(request):
    """
        La funcion vista "Eliminar Archivo" elimina el archivo adjunto cuyo ID se recibe por POST
        @param request:
        @return: Redirecciona a la vista "Consultar User Story".
        """

    if(request.method=='POST'):
        idArchivo = request.POST.get('idArchivo')
        archivo = Archivo_Us.objects.get(id=idArchivo)
        archivo.delete()
        response = {}
        return JsonResponse(response)
    return HttpResponseRedirect('/')

@login_required()
def vista_eliminarNota(request):
    """
        La funcion vista "Eliminar Nota" elimina la nota adjunta cuyo ID se recibe por POST
        @param request:
        @return: Redirecciona a la vista "Consultar User Story".
        """

    if(request.method=='POST'):
        idNota = request.POST.get('idNota')
        nota = Nota_Us.objects.get(id=idNota)
        nota.delete()
        response = {}
        return JsonResponse(response)
    return HttpResponseRedirect('/')