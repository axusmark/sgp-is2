#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from proyecto.forms import *
from proyecto.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib import messages
from django.utils.datastructures import MultiValueDictKeyError


@login_required
def vista_verKanban(request, idP, idFlujo):
    """
        La funcion vista "Ver Kanban" presenta presenta un tablero en donde se detallan el paso de las actividades
        correspondientes a un flujo en particular.
        @param request:
        @param idFlujo: ID del flujo a presentar.
        @param idP: ID del proyecto.
        @return: Renderiza el html encargado de presentar los detalles del tablero.
        """
    proyecto = Proyecto.objects.get(id=idP)
    usuario = Usuario.objects.get(user=request.user)
    # Chutar a usuario sin permisos
    if not usuario.tienePermiso(proyecto, 'ver_kanban'):
        return HttpResponseRedirect('/')
    # obtener permisos de un usuario

    permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
    flujo = get_object_or_404(Flujo_Proyecto, id=idFlujo)

    lista_actividades = Actividad.objects.order_by('orden').filter(flujo=flujo)

    cantActividades = range(1, len(lista_actividades) - 3)

    idUs = Flujo_Us.objects.filter(flujo=flujo).values_list('us_id', flat=True)
    lista_actividad_us = Actividad_Us.objects.order_by('id').filter(us_id__in=idUs)

    #enviar lista de us en los que el usuario es developer
    usuario = Usuario.objects.get(user=request.user)

    idListaUS = Product_Backlog.objects.filter(proyecto=proyecto,usuario=usuario).values_list('us_id', flat=True)
    lista_us_esDeveloper=UserStory.objects.filter(id__in=idListaUS)


    return render(request, "html/kanban/verKanban.html", {'flujo': flujo,
                                                         'lista_actividades': lista_actividades,
                                                         'lista_actividad_us': lista_actividad_us,
                                                        'cantActividades': cantActividades,
                                                         'idP': idP,
                                                         'permisosAsociados': permisosAsociados,
                                                         'proyecto': proyecto,
                                                         'lista_us_esDeveloper': lista_us_esDeveloper,
                                                         })

def vista_listaKanban(request, idP):
    """
        La funcion vista "Lista Kanban" presenta la lista de todos los flujos registrados en el proyecto,
        los cuales tendrán asociados cada uno un tablero Kanban.
        @param request:
        @param idP: id del Proyecto.
        @return: Renderiza el html encargado de presentar la lista de flujos del proyecto.
        """
    proyecto = Proyecto.objects.get(id=idP)
    usuario = Usuario.objects.get(user=request.user)
    # Chutar a usuario sin permisos
    if not usuario.tienePermiso(proyecto, 'ver_lista_kanban'):
        return HttpResponseRedirect('/')
    lista_flujos = Flujo_Proyecto.objects.order_by('id').filter(proyecto=proyecto)
    # obtener permisos de un usuario
    permisosAsociados = proyecto.getListaPermisosDeUsuario(request.user)
    return render(request, "html/kanban/listaFlujos.html", {'lista_flujos': lista_flujos,
                                                            'proyecto': proyecto,
                                                            'idP': idP,
                                                            'permisosAsociados': permisosAsociados,
                                                            })
