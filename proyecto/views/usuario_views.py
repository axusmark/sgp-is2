#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from proyecto.forms import *
from proyecto.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib import messages
import datetime

@login_required
def vista_index(request):
    """
        La funcion vista "Index" redirecciona a la página principal.
        @param request:
        @return: Renderiza el html de la pagina principal.
    """
    if request.user.is_staff:
        cantProyectos= Proyecto.objects.count()
    else:
        usuario=Usuario.objects.get(user=request.user)
        cantProyectos = Usuario_Proyecto.objects.filter(usuario=usuario).distinct('proyecto_id').count()
    cantUsuarios=User.objects.count()

    #obtener proyectos del usuario
    usuario=Usuario.objects.get(user=request.user)
    proyectos = Usuario_Proyecto.objects.filter(usuario=usuario).values_list("proyecto_id", flat=True).distinct('proyecto_id')

    #lista de relaciones del usuario con el proyecto
    psu=Proyecto_Sprint_Us.objects.filter(proyecto_id__in=proyectos)

    #lista de us del usuario
    idUs=psu.values_list("us_id",flat=True)
    logUs = Log_Us.objects.order_by('-id').filter(us_id__in=idUs)[0:10]

    #lista de sprints del usuario
    idSprint=psu.values_list("sprint_id",flat=True)
    logSprint= Log_Sprint.objects.order_by('-id').filter(sprint_id__in=idSprint)[0:10]


    flujos=Flujo_Proyecto.objects.filter(proyecto_id__in=proyectos).values_list("id",flat=True)
    logFlujos=Log_Flujo.objects.order_by('-id').filter(flujo_id__in=flujos)[0:10]
    contexto= {
        'cantProyectos':cantProyectos,
        'cantUsuarios': cantUsuarios,
        'logUs': logUs,
        'logSprint': logSprint,
        'logFlujos': logFlujos,
    }

    return render(request, "index.html",contexto)


@login_required
def vista_verUsuario(request, getIdUsuario):
    """
        La funcion vista "Ver Usuario" presenta un detalle de los datos del usuario.
        @param request:
        @return: Renderiza el html correspondiente, enviando los datos del usuario por contexto.
        """
    user = get_object_or_404(User, id=getIdUsuario)
    usuario= get_object_or_404(Usuario, user=user)
    context = {
        'usuario': usuario,
        'user': user,
    }
    return render(request, "html/usuarios/verUsuario.html", context)

@login_required
def vista_listaUsuarios(request):
    """
        La funcion vista "Lista Usuarios" presenta un listado de todos los usuarios registrados .
        @param request:
        @return: Renderiza el html correspondiente, el cual recibe la lista de usuarios por contexto.
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    lista_usuarios = User.objects.order_by('username').all()
    context = {'lista_usuarios': lista_usuarios}
    return render(request, "html/usuarios/listaUsuarios.html", context)


@login_required
def vista_editarUsuario(request, getIdUsuario):
    """
        La funcion vista "Editar usuario" presenta un formulario para editar los datos actuales del usuario
        recibido por parámetro. Realiza una redirección al actualizarse exitosamente los datos.
        @param request:
        @param getIdUsuario:
        @return: Renderiza el html correspondiente, el cual se encarga de presentar mensajes de confirmación
        de persistencia en la BD y presentar los datos actuales del usuario.
        """
    if request.method=='POST':
        form = FormEditarUsuario(request.POST)
        if form.is_valid():
            datos = form.cleaned_data
            user = User.objects.get(id=request.POST['id'])
            #setear campos de User
            user.username=request.POST['username']
            user.first_name = request.POST['nombre']
            user.last_name = request.POST['apellido']
            user.email = request.POST['email']
            user.save()

            #setear campos de Usuario
            usuario=Usuario.objects.get(user=user)
            usuario.direccion=request.POST['direccion']
            usuario.telefono=request.POST['telefono']
            usuario.save()

            messages.success(request,"Se modifico exitosamente el usuario")
            return HttpResponseRedirect(reverse('proyecto:vista_verUsuario', args=[getIdUsuario]))
    else:
        form = FormEditarUsuario()

    user = get_object_or_404(User, id=getIdUsuario)
    usuario= get_object_or_404(Usuario, user=user)
    context = {'usuario': usuario}
    return render(request, "html/usuarios/editarUsuario.html", context)

@login_required
def vista_cambiarContrasenha(request,getIdUsuario):
    """
        La funcion vista "Cambiar Contraseña" realiza los procedimientos necesarios para cambiar una contraseña
        actual de usuario. Una vez cambiada exitosamente, redirecciona con alerta de confirmación
        @param request:
        @param getIdUsuario:
        @return: Renderiza el html encargado de presentar los detalles del usuario, con una alerta de confirmación.
        """
    msj_error = ''
    user = User.objects.get(id=getIdUsuario)
    usuario = Usuario.objects.get(user=user)

    if user.id == request.user.id:
        if request.method =='POST':
            user = User.objects.get(id=request.POST['id'])
            if user.check_password(request.POST['passwordActual']):

                password1=request.POST['passwordnuevo1']
                password2 = request.POST['passwordnuevo2']

                if password1==password2:
                    request.user.set_password(password1)
                    request.user.save()

                    #msj_ok = "Password actualizado."
                    messages.success(request,"Password actualizado. Vuelva a iniciar sesión con su nuevo password")
                    return HttpResponseRedirect(reverse('autenticacion:vista_login'))
                else:
                    messages.success(request,"Password nuevo no concuerda con el repetido")
            else:
                messages.success(request, "Password actual incorrecto.")

    context = {
        'usuario':usuario,
    }

    return render(request, "html/usuarios/cambiarContrasenha.html",context)



@login_required
def vista_nuevoUsuario(request):
    """
        La funcion vista "Nuevo Usuario" presenta un formulario para ingresar los datos de un nuevo usuario.
        Una vez creado el usuario exitosamente, redirecciona con alerta de confirmación.
        @param request:
        @return: Renderiza el html encargado de presentar los detalles del usuario, con una alerta de confirmación.
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = FormEditarUsuario(request.POST)
        if form.is_valid():
            datos = form.cleaned_data
            user = User()
            # setear campos de User
            user.username = request.POST['username']
            user.first_name = request.POST['nombre']
            user.last_name = request.POST['apellido']
            user.email = request.POST['email']
            user.set_password(request.POST['username'])
            user.save()

            # setear campos de Usuario
            usuario = Usuario(user=user)
            usuario.direccion = request.POST['direccion']
            usuario.telefono = request.POST['telefono']
            usuario.save()

            messages.success(request,"Se creo exitosamente el nuevo usuario")
            return HttpResponseRedirect(reverse('proyecto:vista_listaUsuarios'))
    else:
        form = FormEditarUsuario()

    context = {
        'form':form,

    }
    return render(request,'html/usuarios/crearUsuario.html',context)


@login_required()
def vista_eliminarUsuario(request):
    """
        La funcion vista "Eliminar Usuario" elimina al usuario cuyo ID se recibe por POST.
        @param request:
        @return: Se redirecciona a la vista "Lista Usuarios".
        """
    if(request.method=='POST'):

        idUsuario = request.POST.get('IDUsuario')
        user = User.objects.get(id=idUsuario)
        usuario=Usuario.objects.get(user=user)

        #Se verifica que no sea SM en algun proyecto
        rol = Rol.objects.get(nombre='Scrum Master')
        try:
            lista_proyectos = Usuario_Proyecto.objects.filter(rol=rol,usuario=usuario)
        except Usuario_Proyecto.DoesNotExist:
            lista_proyectos = None

        if lista_proyectos:
            proyectos = "No se puede eliminar el usuario porque es SM en:"
            for x in lista_proyectos:
                proyectos = proyectos +"\n-"+ str(x.proyecto.nombre_corto)
            proyectos = proyectos + "\n\nReasigne el SM de dicho/s proyecto/s e intente de nuevo"
            response = HttpResponse(proyectos)
        else:
            usuario.delete()
            user.delete()
            response = HttpResponse("no")
        #return JsonResponse(response)
        return response
    return HttpResponseRedirect(reverse('sgp:vista_listaUsuarios'))

#Permisos
@login_required
@csrf_protect
def vista_nuevoPermiso(request):
    """
        La funcion vista "Nuevo Permiso" presenta un formulario para ingresar los datos de un nuevo permiso.
        Una vez creado el permiso exitosamente, redirecciona con alerta de confirmación.
        @param request:
        @return: Renderiza el html encargado de presentar los detalles del permiso, con una alerta de confirmación.
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    if request.method != 'POST':
        return render(request, "html/permisos/crearPermiso.html")
    else:
        form = FormEditarPermiso(request.POST)
        if form.is_valid():
            datos = form.cleaned_data
            permiso = Permiso()
            permiso.nombre = datos['nombre']
            permiso.save()

            messages.success(request,"Se creó exitosamente el permiso")
            return HttpResponseRedirect(reverse('proyecto:vista_listaPermisos'))


@login_required
def vista_listaPermisos(request):
    """
        La funcion vista "Lista Permisos" presenta la lista de todos los permisos registrados.
        @param request:
        @return: Renderiza el html encargado de presentar la lista de permisos.
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    lista_permisos = Permiso.objects.order_by('nombre').all()
    context = {'lista_permisos': lista_permisos}
    return render(request, "html/permisos/listaPermisos.html", context)

@login_required
def vista_verPermiso(request, getIdPermiso):
    """
        La funcion vista "Ver Permiso" presenta los detalles de un permiso cuyo id se recibe por parámetro.
        @param request:
        @param getIdPermiso:
        @return: Renderiza el html encargado de presentar los detalles del permiso.
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    permiso = get_object_or_404(Permiso, id=getIdPermiso)
    context = {'permiso': permiso}
    return render(request, "html/permisos/verPermiso.html", context)

@login_required
def vista_editarPermiso(request, getIdPermiso):
    """
        La funcion vista "Editar Permiso" presentar un formulario para modificar los datos actuales de un permiso
        cuyo ID se recibe por parámetro.
        @param request:
        @param getIdPermiso:
        @return: Renderiza el html encargado de presentar los detalles del permiso, con una alerta de confirmación de
        persistencia.
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    if request.method == 'POST':
        form = FormEditarPermiso(request.POST)
        if form.is_valid():
            datos = form.cleaned_data
            permiso = Permiso.objects.get(id=request.POST['id'])
            permiso.nombre=request.POST['nombre']
            permiso.save()
            messages.success(request, "Se modifico exitosamente el permiso")
            return HttpResponseRedirect(reverse('proyecto:vista_listaPermisos'))
    permiso = get_object_or_404(Permiso, id=getIdPermiso)
    context = {'permiso': permiso}
    return render(request, "html/permisos/editarPermiso.html", context)

@login_required()
def vista_eliminarPermiso(request):
    """
        La funcion vista "Eliminar Permiso" elimina el permiso cuyo ID se recibe por POST
        @param request:
        @return: Redirecciona a la vista "Lista Permisos".
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    if(request.method=='POST'):
        idPermiso = request.POST.get('IDPermiso')
        permiso = Permiso.objects.get(id=idPermiso)
        permiso.delete()
        response = {}
        return JsonResponse(response)
    return HttpResponseRedirect(reverse('sgp:vista_listaPermisos'))


#Roles
@login_required
def vista_verRol(request, getIdRol):
    """
        La funcion vista "Ver Rol" presenta los detalles del rol cuyo ID se recibe por parametro
        @param request:
        @param getIdRol:
        @return: Renderiza el html correspondiente, el cual recibe por contexto los datos del rol
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    rol=Rol.objects.get(id=getIdRol)
    idPermisos= Rol_Permiso.objects.filter(rol=rol).values_list('permiso_id',flat=True)
    permisos=Permiso.objects.filter(id__in =idPermisos).values_list('nombre',flat=True)
    context = {'permisos': permisos, 'rol':rol}
    return render(request, "html/roles/verRol.html", context)

@login_required
def vista_nuevoRol(request):
    """
        La funcion vista "Nuevo Rol" presenta un formulario para ingresar los datos de un nuevo usuario.
        @param request:
        @return: Renderiza el html correspondiente. Al finalizar presenta una alerta de confirmación y el detalle
        del nuevo usuario registrado.
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    lista_permisos = Permiso.objects.order_by('nombre').all()
    if request.method != 'POST':
        return render(request, "html/roles/nuevoRol.html",{'lista_permisos': lista_permisos})
    else:
        form = FormEditarPermiso(request.POST)
        if form.is_valid():
            datos = form.cleaned_data
            rol = Rol.objects.create(nombre=datos['nombre'])

            #Guardar permisos seleccionados
            # Obtener nombre de checkboxes chequeados, procesar y guardar
            lista_checks = request.POST.getlist('chkPermisos')
            permisos=[]
            for nombrePermiso in lista_checks:
                permiso = Permiso.objects.get(nombre=nombrePermiso)
                rp = Rol_Permiso(permiso=permiso, rol=rol)
                rp.save()
                permisos.append(permiso.nombre)
            messages.success(request, "Se creó correctamente el rol")
            return HttpResponseRedirect(reverse('proyecto:vista_listaRoles'))


@login_required
def vista_listaRoles(request):
    """
        La funcion vista "Lista Roles" presenta un listado de todos los roles registrados
        @param request:
        @return: Renderiza el html correspondiente, el cual recibe por contexto la lista de roles
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    lista_roles = Rol.objects.order_by('nombre').all()
    context = {'lista_roles': lista_roles}
    return render(request, "html/roles/listaRoles.html", context)

@login_required
def vista_editarRol(request, getIdRol):
    """
        La funcion vista "Editar Rol" presenta un formulario para modificar los datos actuales de un rol
        cuyo ID se recibe por parámetro.
        @param request:
        @param getIdRol:
        @return: Renderiza el html encargado de presentar los detalles del rol, con una alerta de confirmación de
        persistencia.
        """
    #Permitir paso solo a administrador
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = FormEditarRol(request.POST)
        if form.is_valid():
            datos = form.cleaned_data
            rol = Rol.objects.get(id=request.POST['id'])

            #permisos asociados
            #Obtener nombre de checkboxes chequeados
            lista_checks = request.POST.getlist('chkPermisos')
            # Paso 1>> iterar sobre los mismos, por cada checkbox chequeado comprobar
            # si existe en la bd, si no existe entonces guardar en la bd.
            for nombrePermiso in lista_checks:
                permiso = Permiso.objects.get(nombre=nombrePermiso)
                try:
                    rp = Rol_Permiso.objects.get(permiso=permiso, rol=rol)
                except ObjectDoesNotExist:
                    rp = Rol_Permiso(permiso=permiso, rol=rol)
                    rp.save()

            #paso 2>> obtener los permisos asociados actualmente al rol.
            #verificar si cada permiso esta chequeado.
            #Si existe un permiso no chequeado, eliminar de la bd.
            #[1,2,3]
            idPermisos = Rol_Permiso.objects.filter(rol=rol).values_list('permiso_id', flat=True)

            # [permiso1,permiso2,permiso3]
            permisosActuales = Permiso.objects.filter(id__in=idPermisos).values_list('nombre',flat=True)


            for permiso in permisosActuales:
                # verificar si cada permiso actual esta en la lista de checks recibida del form

                if not permiso in lista_checks:
                    permiso = Permiso.objects.get(nombre=permiso)
                    rp = Rol_Permiso.objects.get(permiso=permiso, rol=rol)
                    rp.delete()

                rol.nombre = datos['nombre']
                rol.save()

    #Renderizado inicial
    rol = get_object_or_404(Rol, id=getIdRol)

    #Controlar cuales roles no se pueden eliminar desde el front end
    roles = {'Product Owner', 'Scrum Master', 'Developer'}
    eliminable = False

    #el rol actual es eliminable?
    if not rol.nombre in roles:
        eliminable = True


    #obtener lista de permisos actuales del rol para enviar a los checks del form
    idPermisos = Rol_Permiso.objects.filter(rol=rol).values_list('permiso_id', flat=True)
    permisosActuales = Permiso.objects.filter(id__in=idPermisos).values_list('nombre', flat=True)

    #permisos faltantes
    lista_permisos=Permiso.objects.exclude(nombre__in=permisosActuales)
    if request.method=='POST':
        messages.success(request, "Se actualizó correctamente el rol")
        return HttpResponseRedirect(reverse('proyecto:vista_listaRoles'))
    else:
        return render(request, "html/roles/editarRol.html",
                      {'permisosActuales': permisosActuales, 'rol': rol,
                       'lista_permisos': lista_permisos,
                       'eliminable': eliminable})
@login_required()
def vista_eliminarRol(request):
    """
        La funcion vista "Eliminar Rol" elimina el rol cuyo ID se recibe por POST
        @param request:
        @return: Redirecciona a la vista "Lista Roles".
        """
    if not request.user.is_staff:
        return HttpResponseRedirect('/')
    if(request.method=='POST'):
        idPermiso = request.POST.get('IDRol')
        rol = Rol.objects.get(id=idPermiso)
        #eliminar relacion entre rol y permisos
        rp=Rol_Permiso.objects.filter(rol=rol)
        rp.delete()

        rol.delete()

        response = {}
        return JsonResponse(response)
    return HttpResponseRedirect(reverse('sgp:vista_listaRoles'))



