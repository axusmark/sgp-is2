#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url
#from django.contrib.auth.views import login,logout
from . import views

app_name = 'autenticacion'
urlpatterns = [
    #url(r'^$',views.vista_index, name="vista_index"),
    url(r'^login/$', views.vista_login, name="vista_login"),
    url(r'^logout/$',views.vista_logout, name="vista_logout"),
]