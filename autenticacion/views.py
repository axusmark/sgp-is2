
#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.views.decorators.csrf import csrf_protect
# Create your views here.

@csrf_protect
def vista_login(request):
    """Vista de autenticacion
        solicita USUARIO y PASSWORD al usuario para que se autentique"""
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    mensaje = ' '
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            # password correcto y usuario marcado como "activo"
            auth.login(request, user)
            # Redireccciona a una pagina de entrada correcta.
            return HttpResponseRedirect('/')
        else:
            mensaje = "Password o Username incorrectos"
    return render(request,"autenticacion/login2.html",{'mensaje':mensaje})


def vista_logout(request):
    """Cierra la secion del usuario, mandandolo de vuelta al login"""
    auth.logout(request)
    return HttpResponseRedirect("/login/")